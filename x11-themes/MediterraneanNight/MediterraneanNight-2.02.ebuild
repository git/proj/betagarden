# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

DESCRIPTION="MediterraneanNight GTK 2/3 theme series"
HOMEPAGE="http://gnome-look.org/content/show.php/MediterraneanNight+Series?content=156782"
SRC_URI="https://dl.dropboxusercontent.com/u/80497678/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gtk2 gtk3 linguas_es"

DEPEND=""
RDEPEND="gtk2? ( x11-themes/murrine-themes )
	gtk3? ( >=x11-libs/gtk+-3.6:3 x11-themes/gtk-engines-unico )"

S=${WORKDIR}

src_prepare() {
	rm Mediterranean*/xfwm4/{.gitignore,Makefile} || die
	rm -R Mediterranean*/xfwm4/srcimg || die
}

src_install() {
	dodoc ${PN}/{change-log,readme}.txt
	use linguas_es && dodoc ${PN}/leeme.txt

	local folders=
	use gtk2 && folders+=" gtk-2.0 metacity-1 xfwm4"
	use gtk3 && folders+=" gtk-3.0 unity"

	for i in Mediterranean* ; do
		insinto /usr/share/themes/${i}
		for j in ${folders} ; do
			[[ -e ${i}/${j} ]] || continue
			doins -r ${i}/${j} || die
		done
	done
}
