# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

MY_PV=98756c5e3d15c144ea9872e2ffef9a14507b35b7
DESCRIPTION="Official Ubuntu MATE GTK theme (forked from Moka Project's Orchis GTK theme)"
HOMEPAGE="https://github.com/snwh/yuyo-gtk-theme/"
SRC_URI="https://github.com/snwh/yuyo-gtk-theme/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="x11-themes/gtk-engines-murrine"

S="${WORKDIR}"/${PN}-${MY_PV}

src_install() {
	insinto /usr/share/themes/
	doins -r Yuyo Yuyo-Dark
}
