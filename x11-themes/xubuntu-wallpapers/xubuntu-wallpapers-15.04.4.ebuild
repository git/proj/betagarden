# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit versionator

MY_PN=xubuntu-artwork
DESCRIPTION="Wallpapers distributed with Xubuntu"
HOMEPAGE="http://packages.ubuntu.com/precise/xubuntu-wallpapers"
SRC_URI="http://archive.ubuntu.com/ubuntu/pool/universe/${MY_PN:0:1}/${MY_PN}/${MY_PN}_${PV}.tar.xz"

LICENSE="CC-BY-SA-3.0"
SLOT="$(get_version_component_range 1-2)"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}"/${MY_PN}

src_install() {
	insinto /usr/share/backgrounds/xfce/
	doins usr/share/xfce4/backdrops/*.png

	dodoc debian/copyright
}
