# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

MY_PN=xubuntu-community-artwork
DESCRIPTION="Winners of the Xubuntu 14.04 Community wallpaper contest"
HOMEPAGE="https://wiki.ubuntu.com/Xubuntu/Roadmap/Specifications/Trusty/CommunityWallpapers/Winners"
SRC_URI="http://archive.ubuntu.com/ubuntu/pool/universe/${MY_PN:0:1}/${MY_PN}/${MY_PN}_${PV}.tar.xz"

LICENSE="CC-BY-SA-4.0 CC-BY-3.0 CC0-1.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}"/${MY_PN}-${PV}

src_install() {
	insinto /usr/share/backgrounds/xfce/
	doins usr/share/xfce4/backdrops/*.jpg

	dodoc debian/copyright README
}
