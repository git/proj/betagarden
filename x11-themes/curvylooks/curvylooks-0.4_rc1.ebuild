# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

MY_P="${PN}-${PV/_/-}"
DESCRIPTION="GTK+ 2.x/3.x theme with Clearlooks feeling and Bluecurve-like color scheme"
HOMEPAGE="http://projects.thecodergeek.com/curvylooks/"
SRC_URI="http://projects.thecodergeek.com/${PN}/downloads/${MY_P}.gtp -> ${MY_P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="x11-themes/gnome-themes-standard"

src_prepare() {
	rm Makefile || die
}

src_install() {
	insinto /usr/share/themes/
	doins -r CurvyLooks || die
	dodoc TODO README ChangeLog || die
}
