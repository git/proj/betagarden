# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

DESCRIPTION="GTK+3 3.0 theme based on the Adwaita engine"
HOMEPAGE="http://gnome-look.org/content/show.php/Drakfire+Caffe+GTK3?content=141292"
SRC_URI="http://www.deviantart.com/download/206312449/drakfire_caffe_by_drakfire86-d3etzpd.zip"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND="x11-themes/gnome-themes-standard"

S="${WORKDIR}"

src_prepare() {
	rm Drakfire-Caffe/metacity-1/Makefile.am \
		Drakfire-Caffe/gtk-3.0/*.css~ || die
}

src_install() {
	insinto /usr/share/themes/
	doins -r Drakfire-Caffe || die

	# Stupid workaround to make it appear in GTK+ 2.x choosers
	dodir /usr/share/themes/Drakfire-Caffe/gtk-2.0 || die
	touch "${D}"/usr/share/themes/Drakfire-Caffe/gtk-2.0/gtkrc || die
}
