# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Wallpapers of Linux Mint (both now and past)"
HOMEPAGE="http://packages.linuxmint.com/search.php?keyword=mint-backgrounds"
# Excluded due to duplicate content or branded images, only
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-debian/mint-backgrounds-debian_1.2.tar.gz
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-helena/mint-backgrounds-helena_1.0.tar.gz
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-isadora/mint-backgrounds-isadora_1.0.tar.gz
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-julia/mint-backgrounds-julia_1.0.tar.gz
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-katya/mint-backgrounds-katya_1.0.1.tar.gz
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-lisa/mint-backgrounds-lisa_1.1.tar.gz
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-maya-extra/mint-backgrounds-maya-extra_1.1.tar.gz
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-nadia-extra/mint-backgrounds-nadia-extra_1.0.tar.gz
#	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-xfce/mint-backgrounds-xfce_2014.12.11.tar.gz
SRC_URI="
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-katya-extra/mint-backgrounds-katya-extra_1.0.1.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-lisa-extra/mint-backgrounds-lisa-extra_1.1.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-maya/mint-backgrounds-maya_1.5.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-nadia/mint-backgrounds-nadia_1.4.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-olivia/mint-backgrounds-olivia_1.7.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-petra/mint-backgrounds-petra_1.7.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-qiana/mint-backgrounds-qiana_1.5.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-rafaela/mint-backgrounds-rafaela_1.1.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-rebecca/mint-backgrounds-rebecca_1.5.tar.gz
	http://packages.linuxmint.com/pool/main/m/mint-backgrounds-retro/mint-backgrounds-retro_1.3.tar.gz
	"

LICENSE="
	CC-BY-2.0
	CC-BY-3.0
	CC-BY-SA-2.0
	CC-BY-SA-2.5
	CC-BY-SA-3.0
	CC-BY-SA-4.0
	nonfree? (
		CC-BY-NC-SA-2.5
		CC-BY-ND-2.0
		CC-BY-ND-3.0
		CC-BY-ND-4.0
		all-rights-reserved
	)"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="nonfree"

S="${WORKDIR}"
RESTRICT="mirror bindist"

src_install() {
	insinto /usr/share/backgrounds/
	doins -r */usr/share/backgrounds/* */backgrounds/*

	if ! use nonfree ; then
		(
			cd "${D}"/usr/share/backgrounds/ || die
			rm linuxmint-katya-extra/pr09studio_* || die
			rm linuxmint-lisa-extra/zackh_* || die
			rm linuxmint-olivia/{praia_do_leblon.jpg,wood.jpg} || die
			rm linuxmint-petra/rapciu_* || die
			rm linuxmint-qiana/{9dzign_flame.png,papalars_*,simon__syon_*} || die
			rm linuxmint-rafaela/{ether_*,rapciu_*} || die
			rm linuxmint-rebecca/{papalars_*,simon__syon_*} || die
			rm -R linuxmint-retro/ || die
		) || die
	fi
}
