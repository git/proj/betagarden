# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Theme for GTK 2, GTK 3, Xfwm4 and Metacity"
HOMEPAGE="http://shimmerproject.org/our-projects/bluebird/"
SRC_URI="https://github.com/shimmerproject/${PN^}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="|| ( GPL-2+ CC-BY-SA-3.0 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}"/${PN^}-${PV}

src_install() {
	insinto /usr/share/themes/${PN}
	doins -r .
}
