# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

MY_PV=11053fdeaf05480a0831a8f2caf20fae0bfe0a6b
DESCRIPTION="Theme for GTK 2, GTK 3, Xfwm4 and Metacity"
HOMEPAGE="http://shimmerproject.org/our-projects/bluebird/"
SRC_URI="https://github.com/shimmerproject/${PN^}/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="|| ( GPL-2+ CC-BY-SA-3.0 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}"/${PN^}-${MY_PV}

src_install() {
	insinto /usr/share/themes/${PN}
	doins -r .
}
