# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

inherit versionator

MY_PN=${PN/-/_}
MY_PV=$(replace_all_version_separators '_')
DESCRIPTION="Flat GTK 2/3 theme"
HOMEPAGE="http://gnome-look.org/content/show.php/Siva+Flat+1.3.0?content=156879"
SRC_URI="http://fc07.deviantart.net/fs70/f/2013/057/9/0/${MY_PN}_${MY_PV}_by_nale12-d5ugpl4.zip"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gtk2 gtk3"

DEPEND=""
RDEPEND="gtk2? ( x11-themes/gtk-engines:2 x11-themes/gtk-engines-murrine )
	gtk3? ( x11-themes/gnome-themes-standard x11-themes/gtk-engines-unico )"

S=${WORKDIR}

src_prepare() {
	! use gtk2 && { rm -v */{gtk-2.0,metacity-1,openbox-3,xfwm4} || die ; }
	! use gtk3 && { rm -v */{gtk-3.0,unity} || die ; }
}

src_install() {
	insinto /usr/share/themes/
	doins -r .
}
