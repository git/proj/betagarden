# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils

DESCRIPTION="(Re-)build official Debian OpenStack images"
HOMEPAGE="https://packages.debian.org/sid/openstack-debian-images"
SRC_URI="mirror://debian/pool/main/${PN:0:1}/${PN}/${PN}_${PV}.tar.xz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	app-emulation/qemu
	dev-util/debootstrap
	sys-block/parted
	sys-boot/mbr
	sys-boot/syslinux
	sys-fs/multipath-tools
	"

src_prepare() {
	epatch "${FILESDIR}"/${P}-parted.patch
}

src_install() {
	insinto /usr/share/${PN}/
	doins -r contrib examples

	dodoc debian/changelog

	doman build-openstack-debian-image.1
	dobin build-openstack-debian-image
}
