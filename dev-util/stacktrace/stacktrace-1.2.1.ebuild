# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit multilib eutils

DESCRIPTION="Backtrace wrapper utility and library"
HOMEPAGE="http://www.nopcode.org/wk.php/stacktrace"
SRC_URI="http://kung-foo.net/killabyte/code/${PN}/${P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

src_prepare() {
	epatch "${FILESDIR}"/${P}-ldconfig-path.patch
}

src_install() {
	emake \
		DOCDIR="${D}"/usr/share/doc/${PF} \
		BINDIR="${D}"/usr/bin \
		INCDIR="${D}"/usr/include \
		LIBDIR="${D}"/usr/$(get_libdir) \
		install || die
}
