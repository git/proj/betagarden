# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="UML modeling tool"
HOMEPAGE="http://www.visual-paradigm.com/product/vpuml/"
SRC_URI="http://www.visual-paradigm.com/downloads/vpsuite/VP_Suite_Linux.sh -> ${P}.sh"

LICENSE="Visual-Paradigm-for-UML"
KEYWORDS=""
SLOT="0"
IUSE=""

src_unpack() {
	cp "${DISTDIR}"/${P}.sh "${FILESDIR}"/install_config.xml "${WORKDIR}/" || die "cp failed"
}

src_install() {
	addpredict /home/
	addpredict /root/
	addpredict /usr/local/

	dodir /usr/{bin,lib/vpuml}

	cd "${WORKDIR}" || die
	./${P}.sh -q -dir "${D}/usr/lib/vpuml" || die
	dosym ../lib/vpuml/launcher/run_vpuml /usr/bin/vpuml

	echo -e "edition=Community\nproduct=VP-UML" \
		> "${D}"/usr/lib/vpuml/resources/product_edition.properties \
		|| die "piping to file failed"
}
