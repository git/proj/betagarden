# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="autoconf like tool that allows you to create configure scripts"
HOMEPAGE="http://www.nopcode.org/wk.php/Acr"
SRC_URI="http://www.lolcathost.org/b/${P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_install() {
	emake \
		BINDIR="${D}"/usr/bin \
		MANDIR="${D}"/usr/share/man \
		DATADIR="${D}"/usr/share \
		install
}
