# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Programs for converting diffs"
HOMEPAGE="http://www.mail-archive.com/bug-gnulib@gnu.org/msg09414.html"
SRC_URI="http://www.haible.de/bruno/gnu/${P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

PATCHES=( "${FILESDIR}"/${P}-gcc-4.4.patch )

src_compile() {
	emake -f Makefile-c++
}

src_install() {
	dodoc README
	dobin cd2ud ud2cd cdiffreverse udiffreverse
}
