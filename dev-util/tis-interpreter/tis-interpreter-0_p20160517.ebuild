# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils

MY_PV=4661757d4808e3e301024cc8930de85e5aba05a3
DESCRIPTION="An interpreter for finding subtle bugs in programs written in standard C"
HOMEPAGE="https://github.com/TrustInSoft/tis-interpreter"
SRC_URI="https://github.com/TrustInSoft/tis-interpreter/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="
	dev-lang/ocaml
	dev-ml/biniou
	dev-ml/easy-format
	dev-ml/findlib
	dev-ml/yojson
	dev-ml/zarith
	"
RDEPEND="${DEPEND}"

S="${WORKDIR}"/${PN}-${MY_PV}

src_prepare() {
	epatch "${FILESDIR}"/${P}-path.patch
}

src_install() {
	default

	insinto /usr/lib/${PN}
	doins -r ${PN}/{common_helpers,filesystem}
	newbin ${PN}/${PN}.sh ${PN}
	dobin filesystem/tis-mkfs

	insinto /usr/share/${PN}
	doins -r ${PN}/examples
}
