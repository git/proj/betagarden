# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Flexible wrapper around debootstrap"
HOMEPAGE="https://github.com/grml/grml-debootstrap"
SRC_URI="https://github.com/grml/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND="
	app-text/asciidoc
	app-text/docbook-xsl-stylesheets
	dev-libs/libxslt
	"
RDEPEND="
	app-emulation/qemu
	app-shells/mksh
	dev-util/dialog
	dev-util/debootstrap
	sys-block/parted
	app-crypt/debian-archive-keyring
	app-crypt/ubuntu-keyring
	"

src_compile() {
	emake DOCBOOK_XML=/usr/share/sgml/docbook/xsl-stylesheets/manpages/docbook.xsl doc_man
	use doc && emake doc_html
}

src_install() {
	emake DESTDIR="${D}" install

	dodoc TODO
	doman ${PN}.8

	# Some doc on top
	use doc && {
		dodoc grml-debootstrap.8.html
		insinto /usr/share/doc/${PF}/
		doins -r images
	}

	dodoc debian/changelog
}
