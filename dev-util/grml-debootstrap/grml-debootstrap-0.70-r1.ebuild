# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Flexible wrapper around debootstrap"
HOMEPAGE="https://github.com/grml/grml-debootstrap"
SRC_URI="mirror://debian/pool/main/${PN:0:1}/${PN}/${PN}_${PV}.tar.xz"

LICENSE="GPL-2"  # not GPL-2+ since the config file is GPL-2 only
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND="
	app-text/asciidoc
	app-text/docbook-xsl-stylesheets
	dev-libs/libxslt
	"
RDEPEND="
	app-emulation/qemu
	app-shells/mksh
	dev-util/dialog
	dev-util/debootstrap
	sys-block/parted
	app-crypt/debian-archive-keyring
	app-crypt/ubuntu-keyring
	"

src_prepare() {
	local old=/usr/share/xml/docbook/stylesheet/nwalsh/manpages/docbook.xsl
	local new=/usr/share/sgml/docbook/xsl-stylesheets/manpages/docbook.xsl
	sed "s,${old},${new}," -i Makefile || die
}

src_compile() {
	emake doc_man
	use doc && emake doc_html
}

src_install() {
	# debian/rules target "install"
	dodir etc/debootstrap usr/sbin etc/debootstrap/extrapackages \
			etc/zsh/completion.d usr/share/grml-debootstrap/functions
	install -m 644 config           "${D}"/etc/debootstrap/
	install -m 644 devices.tar.gz   "${D}"/etc/debootstrap/
	install -m 644 locale.gen       "${D}"/etc/debootstrap/
	install -m 644 packages         "${D}"/etc/debootstrap/
	install -m 755 chroot-script    "${D}"/etc/debootstrap/
	install -m 755 grml-debootstrap "${D}"/usr/sbin/
	install -m 644 zsh-completion   "${D}"/etc/zsh/completion.d/_grml-debootstrap
	install -m 644 cmdlineopts.clp  "${D}"/usr/share/grml-debootstrap/functions/cmdlineopts.clp
	install -m 755 bootgrub.mksh    "${D}"/usr/share/grml-debootstrap/bootgrub.mksh

	# debian/rules target "binary-indep"
	dodoc TODO THANKS
	doman ${PN}.8

	# Some doc on top
	use doc && {
		dodoc grml-debootstrap.8.html
		insinto /usr/share/doc/${PF}/
		doins -r images
	}

	dodoc debian/changelog
}
