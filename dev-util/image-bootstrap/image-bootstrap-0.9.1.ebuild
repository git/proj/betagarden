# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

PYTHON_COMPAT=( python2_7 )
inherit distutils-r1

DESCRIPTION="Command line tool for creating bootable virtual machine images"
HOMEPAGE="https://github.com/hartwork/image-bootstrap"
SRC_URI="https://github.com/hartwork/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="AGPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

# yes, setuptools is used during runtime
RDEPEND="
	app-crypt/gnupg
	dev-python/beautifulsoup[${PYTHON_USEDEP}]
	dev-python/colorama[${PYTHON_USEDEP}]
	dev-python/requests[${PYTHON_USEDEP}]
	dev-python/setuptools[${PYTHON_USEDEP}]
	>=dev-util/debootstrap-1.0.78-r1
	sys-boot/grub:2
	sys-boot/mbr
	sys-boot/syslinux
	sys-fs/multipath-tools
	sys-block/parted
	"
