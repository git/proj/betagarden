# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit eutils python-single-r1

DESCRIPTION="Maintenance of a Project"
HOMEPAGE="http://thomas.apestaart.org/moap/trac"
SRC_URI="http://thomas.apestaart.org/download/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test doc"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="
	doc? ( >=dev-python/epydoc-2.1-r2 )
	test? ( >=dev-python/pychecker-0.8.17 )
"
RDEPEND="
	>=dev-libs/redland-bindings-1.0.7.1[python]
	${PYTHON_DEPS}"

#Working on issues with tests/docs:
#http://thomas.apestaart.org/moap/trac/ticket/287
