# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"

inherit autotools

DESCRIPTION="glibc 2.4+ LD_AUDIT feature frontend"
HOMEPAGE="http://people.redhat.com/jolsa/latrace/index.shtml"
SRC_URI="http://people.redhat.com/jolsa/${PN}/dl/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-text/asciidoc
	sys-devel/bison
	sys-devel/flex
	app-text/xmlto"
RDEPEND=""

src_prepare() {
	eautoreconf
}

src_compile() {
	emake -j1 || die
}

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc ReleaseNotes README ChangeLog || die
}
