# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit eutils toolchain-funcs

DESCRIPTION="Compiler that produces program analysis information, an alternative to cflow or cscope"
HOMEPAGE="http://students.ceid.upatras.gr/~sxanth/ncc/"
SRC_URI="http://students.ceid.upatras.gr/~sxanth/${PN}/${P}.tar.gz"

LICENSE="Artistic"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

src_prepare() {
	epatch "${FILESDIR}"/${P}-flags.patch
}

src_compile() {
	emake \
			-C nccnav \
			CC=$(tc-getCXX) \
			CFLAGS="${CFLAGS} -c" \
			LCFLAGS="${CFLAGS} ${LDFLAGS}" \
			-j1 \
		|| die

	emake \
			CC=$(tc-getCXX) \
			LCFLAGS="${CFLAGS} ${LDFLAGS}" \
			-j1 \
		|| die
}

src_install() {
	dodir \
			/usr/bin \
			/usr/include \
			/usr/share/man/man1 \
		|| die

	emake DESTDIR="${D}"/usr install || die

	dodoc doc/{CHANGES,TROUBLES,hacking.*,int8_t.redefined.error,KEYS.txt,NCC} || die
	insinto /usr/share/doc/${PF}/
	doins doc/*.c || die
	newdoc nccnav/README NCCNAV || die

	newbin scripts/gengraph.py nccgengraph || die
	newman scripts/gengraph.py.1 nccgengraph.1 || die
}
