# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit git-r3

DESCRIPTION="Find orphaned files for unmerged packages"
HOMEPAGE="http://git.xnull.de/cgit/findcruft2/"
EGIT_REPO_URI="git://git.xnull.de/findcruft2.git"
EGIT_COMMIT="aaff6e79079764f7f3b4e1bbebbeb88a13f3267d"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="
	app-shells/bash:*
	sys-apps/coreutils
	sys-apps/findutils
	sys-apps/grep
	sys-apps/sed
"

src_install() {
	dosbin findcruft
	insinto /etc/findcruft
	doins -r etc/*
}
