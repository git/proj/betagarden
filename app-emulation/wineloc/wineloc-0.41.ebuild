# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="CJK Launcher for Wine (cli)"
HOMEPAGE="https://bitbucket.org/dsobodash/winelocale"
SRC_URI="http://files.sobodash.com/software/winelocale/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="
	app-emulation/wine
	media-fonts/VeraSansYuanTi"

src_prepare() {
	sed \
		-e 's|WLOCSHARE=/usr/local/share/wineloc|WLOCSHARE=/usr/share/wineloc|' \
		-i share/wineloc || die
}

src_install() {
	insinto /usr/share/${PN}
	doins -r share/patches

	dodoc CHANGELOG INSTALL

	dobin share/wineloc

	elog 'For Chinese, be sure to enable these locales in /etc/locale.gen:'
	elog '  zh_CN.UTF-8 UTF-8'
	elog '  zh_TW.UTF-8 UTF-8'
	elog '  zh_CN GB2312'
	elog '  zh_CN.GBK GBK'
	elog '  zh_CN.GB18030 GB18030'
	elog '  zh_TW BIG5'
	elog ''
	elog 'Do not forget to run "locale-gen" to propagate changes.'
}
