# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools

DESCRIPTION="Slide show presentation tool"
HOMEPAGE="http://code.google.com/p/xsw/"
SRC_URI="http://${PN}.googlecode.com/files/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE="pdf"

DEPEND="
	media-libs/libsdl
	media-libs/sdl-ttf
	media-libs/sdl-image
	media-libs/sdl-gfx"
RDEPEND="${DEPEND}
	pdf? ( media-gfx/imagemagick )"

PATCHES=( "${FILESDIR}"/${PN}-0.3.5-fix-destdir-remove-usr-local-stuff.patch )

src_prepare() {
	default
	eautomake
}

src_install() {
	default
	dodoc REFERENCE
}
