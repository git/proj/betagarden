# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="A simple dual-screen PDF reader designed for presentations"
HOMEPAGE="http://pympress.org/"
SRC_URI="https://github.com/Cimbali/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-python/python-poppler[${PYTHON_USEDEP}]
	dev-python/python-vlc[${PYTHON_USEDEP}]
	dev-python/pygtk[${PYTHON_USEDEP}]"
DEPEND="${RDEPEND}"
