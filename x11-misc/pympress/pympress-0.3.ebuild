# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="A simple dual-screen PDF reader designed for presentations"
HOMEPAGE="http://pympress.org/"
SRC_URI="https://github.com/Schnouki/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="dev-python/python-poppler[${PYTHON_USEDEP}]
	dev-python/pygtk[${PYTHON_USEDEP}]"
DEPEND="${RDEPEND}"
