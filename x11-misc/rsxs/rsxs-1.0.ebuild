# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit multilib

DESCRIPTION="GLX port of Really Slick Screensavers; can be used with XScreensaver"
HOMEPAGE="http://sourceforge.net/projects/rsxs/"
SRC_URI="mirror://sourceforge/project/${PN}/${PN}/${PV}/${P}.tar.gz"

LICENSE="GPL-3"  # no "or later"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug"

DEPEND="
	media-libs/libpng
	media-libs/libvorbis
	media-libs/glu
	media-libs/mesa
	media-libs/openal
	x11-libs/libXmu
	x11-libs/libX11
	"
RDEPEND="${DEPEND}"

src_prepare() {
	# Fix compilation
	# http://sourceforge.net/p/openil/bugs/203/#5e3f
	sed 's,png_set_gray_1_2_4_to_8,png_set_expand_gray_1_2_4_to_8,g' \
			-i src/pngimage.cc || die
}

src_configure() {
	econf \
		--with-hackdir=/usr/$(get_libdir)/misc/xscreensaver/ \
		--with-moduledir=/usr/share/xscreensaver/config/ \
		$(use_enable debug)
}
