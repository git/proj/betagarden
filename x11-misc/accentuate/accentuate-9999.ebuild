# Copyright 1999-2013XXX Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit git-r3 python-r1

DESCRIPTION="PDF Slideshow Presentation Software"
HOMEPAGE="http://repo.or.cz/w/accentuate.git"
SRC_URI=""
EGIT_REPO_URI="git://repo.or.cz/accentuate.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="lirc"

DEPEND="
	dev-python/pygame[${PYTHON_USEDEP}]
	dev-python/imaging[${PYTHON_USEDEP}]
	lirc? ( dev-python/pylirc[${PYTHON_USEDEP}] )"
RDEPEND="${DEPEND}"

src_install() {
	python_foreach_impl python_doscript ${PN}
}
