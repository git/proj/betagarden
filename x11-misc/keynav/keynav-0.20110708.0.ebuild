# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit toolchain-funcs

DESCRIPTION="Move the mouse pointer with few key strokes"
HOMEPAGE="http://www.semicomplete.com/projects/keynav/"
SRC_URI="https://semicomplete.googlecode.com/files/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	x11-libs/cairo[X]
	x11-libs/libXinerama
	dev-libs/glib:2
	x11-libs/libXext
	x11-libs/libX11
	x11-misc/xdotool
	"
DEPEND="${RDEPEND}"

src_compile() {
	tc-export CC LD
	default
}

src_install() {
	dodoc README CHANGELIST
	dobin keynav

	insinto /etc
	doins keynavrc
}
