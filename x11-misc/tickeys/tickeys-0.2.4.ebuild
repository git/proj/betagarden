# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

MY_PN=${PN^}-linux
DESCRIPTION="Instant audio feedback when typing."
HOMEPAGE="https://github.com/BillBillBillBill/Tickeys-linux"
SRC_URI="https://github.com/BillBillBillBill/${MY_PN}/archive/v${PV}.tar.gz
	-> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="${PYTHON_DEPS}"
RDEPEND="${DEPEND}
	dev-python/cython[${PYTHON_USEDEP}]
	dev-python/Kivy[${PYTHON_USEDEP}]
	dev-python/kivy-garden[${PYTHON_USEDEP}]
	dev-python/notify2[${PYTHON_USEDEP}]
	dev-python/pygame[${PYTHON_USEDEP}]
	dev-python/python-evdev[${PYTHON_USEDEP}]
	"

S="${WORKDIR}"/${MY_PN}-${PV}

src_compile() {
	# https://github.com/BillBillBillBill/Tickeys-linux/issues/14
	rm /tmp/tickeys.log  # or we still get sandbox errors
	addpredict /tmp/tickeys.log

	distutils-r1_src_compile
}
