# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit eutils distutils-r1

DESCRIPTION="Instant audio feedback when typing."
HOMEPAGE="https://github.com/BillBillBillBill/Tickeys-linux"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="${PYTHON_DEPS}"
RDEPEND="${DEPEND}
	dev-python/cython[${PYTHON_USEDEP}]
	dev-python/Kivy[${PYTHON_USEDEP}]
	dev-python/kivy-garden[${PYTHON_USEDEP}]
	dev-python/notify2[${PYTHON_USEDEP}]
	dev-python/pygame[${PYTHON_USEDEP}]
	dev-python/python-evdev[${PYTHON_USEDEP}]
	"

src_prepare() {
	epatch "${FILESDIR}"/${P}-requirements.patch
}

src_compile() {
	# https://github.com/BillBillBillBill/Tickeys-linux/issues/14
	rm /tmp/tickeys.log  # or we still get sandbox errors
	addpredict /tmp/tickeys.log

	distutils-r1_src_compile
}
