# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit versionator vala gnome2-utils cmake-utils

DESCRIPTION="Animated OpenGL wallpaper"
HOMEPAGE="https://launchpad.net/livewallpaper"
SRC_URI="https://launchpad.net/livewallpaper/$(get_version_component_range 1-2)/${PV}/+download/${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-libs/glib:2
	dev-libs/gobject-introspection
	dev-libs/libpeas
	media-libs/glew:*
	media-libs/mesa
	sys-libs/glibc
	sys-power/upower
	x11-libs/cairo
	x11-libs/gdk-pixbuf
	x11-libs/gtk+:3
	x11-libs/libX11
	x11-libs/pango
	"
DEPEND="
	${RDEPEND}
	$(vala_depend)
	media-gfx/xcftools
	virtual/pkgconfig
	"

src_prepare() {
	sed "s,valac,valac-$(vala_best_api_version),g" -i cmake/FindVala.cmake || die
	sed '/^add_subdirectory(debian)$/d' -i CMakeLists.txt || die
	cmake-utils_src_prepare
	eapply_user
}

pkg_preinst() {
	gnome2_schemas_savelist
}

pkg_postinst() {
	gnome2_schemas_update
}

pkg_postrm() {
	gnome2_schemas_update
}
