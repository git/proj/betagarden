# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="CRT and LCD screen testing utility"
HOMEPAGE="http://screentest.sourceforge.net/"
SRC_URI="mirror://sourceforge/project/${PN}/${PN}/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-libs/glib
	gnome-base/libglade
	x11-libs/gtk+
	"
RDEPEND="${DEPEND}"

src_configure() {
	LIBS="-lgmodule-2.0" default
}
