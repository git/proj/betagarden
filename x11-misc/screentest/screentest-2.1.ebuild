# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="CRT and LCD screen testing utility"
HOMEPAGE="https://tobix.github.io/screentest/"
SRC_URI="https://github.com/TobiX/screentest/releases/download/${PV}/screentest-${PV}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-libs/glib-2.36.0
	>=x11-libs/gtk+-2.16.0:2
	"
DEPEND="${RDEPEND}
	>=dev-util/intltool-0.40.0
	"
