# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python3_{4,5,6} )
inherit python-any-r1

MY_PV=${PV#0_p}
DESCRIPTION="Force users (gently) to take periodical breaks"
HOMEPAGE="https://launchpad.net/takeabreak"
SRC_URI="http://bazaar.launchpad.net/~vlijm/${PN}/trunk/tarball/${MY_PV} -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="${PYTHON_DEPS}
	x11-apps/xrandr
	x11-libs/libnotify
	x11-misc/xscreensaver
	"

S="${WORKDIR}"/~vlijm/${PN}/trunk

PATCHES=(
	"${FILESDIR}"/${P}-xscreensaver.patch
)

src_install() {
	doman manpages/${PN}.1

	insinto /usr/share/applications
	doins miscellaneous/extras-${PN}.desktop

	insinto /opt/${PN}  # upstream's idea...
	doins -r docs icon

	exeinto /opt/${PN}/${PN}  # again, upstream's idea
	doexe code/*
}
