# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit autotools git-r3

DESCRIPTION="Linux WebQQ Client"
HOMEPAGE="https://github.com/mathslinux/lwqq"
EGIT_REPO_URI="https://github.com/mathslinux/lwqq.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="sys-libs/zlib
	dev-libs/libev
	x11-libs/gtk+:3
	dev-libs/glib:2
	dev-db/sqlite:3"
DEPEND="${DEPEND}"

src_prepare() {
	eautoreconf
}
