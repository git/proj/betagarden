# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit git-r3 cmake-utils

DESCRIPTION="webqq protocol library"
HOMEPAGE="https://github.com/xiehuc/lwqq"
EGIT_REPO_URI="https://github.com/xiehuc/lwqq.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="net-misc/curl
	dev-db/sqlite:3
	sys-libs/zlib
	dev-libs/libev
	dev-libs/libuv
	!net-im/lwqq"
DEPEND="virtual/pkgconfig
	${RDEPEND}"
