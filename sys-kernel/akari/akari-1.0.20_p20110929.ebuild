# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=4

inherit linux-mod

MY_P=${P/_p/-}

DESCRIPTION="A security module for system analysis and protection"
HOMEPAGE="http://akari.sourceforge.jp/index.html.en"
SRC_URI="mirror://sourceforge.jp/akari/49272/${MY_P}.tar.gz"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~amd64"
IUSE=""

S="${WORKDIR}"/${PN}

MODULE_NAMES="akari(security)"
BUILD_PARAMS="-C ${KERNEL_DIR} M=${S}"
BUILD_TARGETS="modules"
