# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit versionator mount-boot unpacker

MY_PV="$(get_version_component_range 1-3)-$(get_version_component_range 4)"
PATCH_PV="$(get_version_component_range 5-7)-$(get_version_component_range 8)"

DESCRIPTION="Linux kernel and modules for AMD64, Intel 64 and VIA Nano processors"
HOMEPAGE="https://packages.debian.org/sid/linux-image-amd64"
SRC_URI="mirror://debian/pool/main/l/linux-signed/linux-image-${MY_PV}-amd64_${PATCH_PV}_amd64.deb"

LICENSE="GPL-2"
SLOT="$(get_version_component_range 1-3)"
KEYWORDS="~amd64"
IUSE=""

S="${WORKDIR}"/

src_install() {
	insinto /usr/src/linux-${SLOT}-debian
	newins boot/config-* .config

	insinto /
	doins -r boot lib
}
