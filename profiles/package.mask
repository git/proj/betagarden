~sys-apps/dbus-9999
~sys-block/compcache-9999

# 2011-06-06  Sebastian Pipping <sping@gentoo.org>
# Experimental downstream patches
=media-gfx/cataract-1.1.0_p20110326_p2

# 2017-08-07  Sebastian Pipping <sping@gentoo.org>
# Error "Couldn't mmap v8 natives data file, status code is 1"
# during start-up; caused by these files missing from
# recent sid chromium*.deb files; needs further investigation
# - /usr/lib/chromium/natives_blob.bin
# - /usr/lib/chromium/snapshot_blob.bin
# - /usr/lib/chromium/v8_context_snapshot.bin
=www-client/chromium-bin-debian-60.0.3112.78_p1
=www-client/chromium-bin-debian-63.0.3239.84_p1
