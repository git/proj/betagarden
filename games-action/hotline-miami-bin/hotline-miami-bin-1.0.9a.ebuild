# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

inherit games

MY_PN=${PN%%-bin}
DESCRIPTION="Top-down action game, a bit like GTA 1 without cars"
HOMEPAGE="http://hotlinemiami.com/"
SRC_URI="${MY_PN/-/}_v${PV}-Linux_28-05-2013.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

make_dep() {
	package=$1
	echo "amd64? ( ${package}[abi_x86_32] )  x86? ( ${package} )"
}

DEPEND=""
RDEPEND="amd64? (
		app-emulation/emul-linux-x86-opengl
		app-emulation/emul-linux-x86-soundlibs
	)
	x86? (
		media-libs/mesa
		media-libs/glu
		media-libs/libogg
		media-libs/libvorbis
	)
	$(make_dep media-libs/fontconfig)
	$(make_dep media-libs/freetype)
	$(make_dep x11-libs/libX11)
	$(make_dep x11-libs/libxcb)
	$(make_dep x11-libs/libXext)
	$(make_dep x11-libs/libXrender)
	$(make_dep x11-libs/libXrandr)"

S=${WORKDIR}
RESTRICT="bindist fetch mirror strip"

QA_PREBUILT="usr/games/${MY_PN}/lib/*"

src_install() {
	local game_home=${GAMES_PREFIX}/${MY_PN}

	# Install all shipped files
	dodir "${game_home}"
	cp -R * "${D}/${game_home}"/ || die

	# Make launcher
	dodir "${GAMES_BINDIR}"
	cat <<-EOF >>"${D}/${GAMES_BINDIR}"/${MY_PN}
		#! /bin/bash
		set -x
		cd '${game_home}' && ./hotline_launcher
	EOF
}
