# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit autotools eutils games

MY_PN="supertuxkart"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="A kart racing game starring Tux, the linux penguin (TuxKart fork)"
HOMEPAGE="http://supertuxkart.sourceforge.net/"
SRC_URI="
	mirror://sourceforge/supertuxkart/files/SuperTuxKart/${PV}/${MY_P}-src.tar.bz2
	mirror://gentoo/${MY_PN}.png"

LICENSE="GPL-3 CC-BY-SA-3.0 CC-BY-2.0 public-domain"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="nls"

RDEPEND=">=media-libs/plib-1.8.4
	virtual/opengl
	media-libs/freeglut
	virtual/glu
	net-libs/enet:0
	media-libs/libmikmod
	media-libs/libvorbis
	media-libs/openal
	media-libs/libsdl[X,video,audio,joystick]
	virtual/libintl"
DEPEND="${RDEPEND}
	!games-action/supertuxkart
	nls? ( sys-devel/gettext )"

S="${WORKDIR}/${MY_P}"

src_prepare() {
	esvn_clean
	sed -i \
		-e '/ENETTREE/d' \
		-e '/src\/enet\/Makefile/d' \
		-e '110 i\   AC_SEARCH_LIBS(gluLookAt, GLU)' \
		configure.ac \
		|| die "sed failed"
	sed -i \
		-e '/SUBDIRS/s/doc//' \
		-e '/pkgdata/d' \
		Makefile.am \
		|| die "sed failed"
	sed -i \
		-e '/pkgdatadir/s:/games::' \
		-e '/desktop/d' \
		-e '/icon/d' \
		$(find data -name Makefile.am) \
		|| die "sed failed"
	sed -i \
		-e '/bindir/d' \
		-e '/AM_CPPFLAGS/s:/games::' \
		src/Makefile.am \
		|| die "sed failed"
	# bug #328021
	sed -i \
		-e '13d' \
		data/Makefile.am \
		|| die "sed failed"
	rm -rf src/enet
	eautoreconf

	epatch "${FILESDIR}"/${MY_P}-ovflfix.patch \
			"${FILESDIR}"/${MY_P}-speedhack.patch
}

src_configure() {
	egamesconf \
		--disable-dependency-tracking \
		$(use_enable nls)
}

src_install() {
	default
	doicon "${DISTDIR}"/${MY_PN}.png
	make_desktop_entry ${MY_PN} SuperTuxKart
	dodoc AUTHORS ChangeLog README TODO
	prepgamesdirs
}
