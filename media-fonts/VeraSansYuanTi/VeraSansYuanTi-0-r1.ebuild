# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Chinese font Vera Sans Yuan Ti (including Mono)"
HOMEPAGE=""
SRC_URI="
	!minimal? (
		ftp://ftp.bupt.edu.cn/pub/software/Chinese/Font-related/VeraSans/VeraSansYuanTi-Bold.zip
		ftp://ftp.bupt.edu.cn/pub/software/Chinese/Font-related/VeraSans/VeraSansYuanTi-BoldItalic.zip
		ftp://ftp.bupt.edu.cn/pub/software/Chinese/Font-related/VeraSans/VeraSansYuanTi-Italic.zip
	)
	ftp://ftp.bupt.edu.cn/pub/software/Chinese/Font-related/VeraSans/VeraSansYuanTi-Regular.zip
	!minimal? (
		ftp://ftp.bupt.edu.cn/pub/software/Chinese/Font-related/VeraSans/VeraSansYuanTiMono-Bold.zip
		ftp://ftp.bupt.edu.cn/pub/software/Chinese/Font-related/VeraSans/VeraSansYuanTiMono-BoldItalic.zip
		ftp://ftp.bupt.edu.cn/pub/software/Chinese/Font-related/VeraSans/VeraSansYuanTiMono-Italic.zip
	)
	ftp://ftp.bupt.edu.cn/pub/software/Chinese/Font-related/VeraSans/VeraSansYuanTiMono-Regular.zip
	"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="minimal"

RESTRICT=""

S="${WORKDIR}"

src_install() {
	insinto /usr/share/fonts/${PN}
	doins *.ttf || die
}
