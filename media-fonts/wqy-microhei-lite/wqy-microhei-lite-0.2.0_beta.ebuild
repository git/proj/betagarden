# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit font

MY_PV=${PV/_/-}
DESCRIPTION="Chinese font"
HOMEPAGE="http://sourceforge.net/projects/wqy/"
SRC_URI="mirror://sourceforge/project/wqy/${PN}/${MY_PV}/${PN}-${MY_PV}.tar.gz"

LICENSE="|| ( Apache-2.0 GPL-3-with-font-exception )"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE=""

S="${WORKDIR}"

src_install() {
	insinto ${FONTDIR}
	newins ${PN}/${PN}.ttc ${PN}.ttf || die

	dodoc ${PN}/{AUTHORS,ChangeLog,README}.txt || die
}
