# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"
inherit eutils mercurial pam toolchain-funcs

DESCRIPTION="Example PAM module demonstrating two-factor authentication"
HOMEPAGE="http://code.google.com/p/google-authenticator/"
SRC_URI=""
EHG_REPO_URI="https://google-authenticator.googlecode.com/hg/libpam/"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="test"

DEPEND="sys-libs/pam"
RDEPEND="${DEPEND}
	media-gfx/qrencode"

S="${WORKDIR}/libpam"

src_unpack() {
	mercurial_fetch "${EHG_REPO_URI}" "$(basename "${EHG_REPO_URI}")" "${WORKDIR}"
}

src_prepare() {
	epatch "${FILESDIR}/${P}-as-needed.patch"
}

src_compile() {
	emake CC="$(tc-getCC)"
}

src_install() {
	dopammod pam_google_authenticator.so

	dobin google-authenticator

	dodoc README
	dohtml totp.html
}
