# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit autotools

DESCRIPTION="NBD server program enabling live block device migration over wide-area networks"
HOMEPAGE="https://bitbucket.org/hirofuchi/xnbd/wiki/Home"
SRC_URI="https://bitbucket.org/hirofuchi/xnbd/downloads/${P}.tgz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-libs/glib-2.32:2
	dev-libs/jansson"
DEPEND="${RDEPEND}
	app-text/asciidoc"

S=${WORKDIR}/${P}/trunk

src_prepare() {
	eautoreconf
}
