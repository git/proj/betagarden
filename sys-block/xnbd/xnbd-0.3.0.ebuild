# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

MY_P=${PN}-${PV/_/-}

DESCRIPTION="NBD server program enabling live block device migration over wide-area networks"
HOMEPAGE="https://bitbucket.org/hirofuchi/xnbd/wiki/Home"
SRC_URI="https://bitbucket.org/hirofuchi/xnbd/downloads/${MY_P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-libs/glib-2.32:2"
DEPEND="${RDEPEND}"

S=${WORKDIR}/${MY_P}
