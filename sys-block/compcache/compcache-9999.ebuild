# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"
inherit eutils linux-mod mercurial toolchain-funcs

DESCRIPTION="Compressed RAM as fast swap"
HOMEPAGE="http://compcache.googlecode.com/"
SRC_URI=""
#SRC_URI="http://compcache.googlecode.com/files/${P}.tar.gz"
EHG_REPO_URI="https://compcache.googlecode.com/hg/"

LICENSE="GPL-2 LGPL-2.1"
KEYWORDS="~amd64 ~arm ~x86"
IUSE=""

DEPEND="virtual/linux-sources"
RDEPEND=""

S="${WORKDIR}/hg"

BUILD_PARAMS='KV_OUT_DIR="${KV_OUT_DIR}"'
BUILD_TARGETS="all"
CONFIG_CHECK="LZO_COMPRESS LZO_DECOMPRESS SWAP"
MODULE_NAMES="zram(compcache:${S}:${S})"
MODULESD_ZRAM_DOCS="Changelog README"
MODULESD_ZRAM_EXAMPLES=('zram num_devices=4')

src_prepare() {
	find . -name Makefile -exec sed -i \
		-e 's:make:$(MAKE):g' \
		-e "s:@gcc:$(tc-getCC):g" \
		-e 's#/lib/modules/$(shell uname -r)/build#"$(KV_OUT_DIR)"#' \
		{} \;
}
