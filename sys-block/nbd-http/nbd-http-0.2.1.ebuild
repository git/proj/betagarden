# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit eutils toolchain-funcs

DESCRIPTION="Read a file over HTTP as a block device."
HOMEPAGE="http://patraulea.com/nbd-http/"
SRC_URI="http://patraulea.com/${PN}/src/${P}.tgz"

LICENSE="all-rights-reserved"  # i.e. not specific
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

src_prepare() {
	epatch "${FILESDIR}"/${P}-{compile,ldflags,warnings}.patch
}

src_compile() {
	emake CC=$(tc-getCC) CFLAGS="${CFLAGS}" || die
}

src_install() {
	dodoc ChangeLog README || die
	dobin ${PN} || die
}
