# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit autotools eutils vcs-snapshot

DESCRIPTION="A dovecot metadata plugin"
HOMEPAGE="http://hg.dovecot.org/dovecot-metadata-plugin"
SRC_URI="http://hg.dovecot.org/dovecot-metadata-plugin/archive/v${PV}.tar.bz2 -> ${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=net-mail/dovecot-2.2.0"
RDEPEND="${DEPEND}"

src_prepare() {
	eautoreconf
}
