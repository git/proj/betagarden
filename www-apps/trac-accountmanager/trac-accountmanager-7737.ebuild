# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="An account manager plugin for Trac"
HOMEPAGE="http://trac-hacks.org/wiki/AccountManagerPlugin"
SRC_URI="http://trac-hacks.org/changeset/${PV}/accountmanagerplugin?old_path=/&format=zip -> ${P}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-python/setuptools[${PYTHON_USEDEP}]
	app-arch/unzip"
RDEPEND=">=www-apps/trac-0.12"

S="${WORKDIR}/accountmanagerplugin/trunk"

python_install_all() {
	distutils-r1_python_install_all
	rm -f "${D}"/usr/{README,COPYING}
}
