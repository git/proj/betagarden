# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

inherit git-r3 autotools

DESCRIPTION="Software Defined Radio project"
HOMEPAGE="http://sdr.osmocom.org/trac/"
SRC_URI=""

EGIT_REPO_URI="git://git.osmocom.org/osmo-sdr"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-doc/doxygen
	dev-util/cmake"
RDEPEND=""

src_unpack() {
	git-r3_src_unpack
	S="${WORKDIR}"/${P}/software/${PN}/
}

src_prepare() {
	eautoreconf
}

src_install() {
	default
	mv "${D}"/usr/share/doc/{${PN}/${PN}-UNKNOWN,${PF}}/html || die
	rmdir "${D}"/usr/share/doc/${PN}{/${PN}-UNKNOWN,} || die
}
