# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

inherit autotools

DESCRIPTION="Software Defined Radio project"
HOMEPAGE="http://sdr.osmocom.org/trac/"
SRC_URI="http://hartwork.org/public/osmo-sdr-hopscotch-${PV}.tar.bz2"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-doc/doxygen
	dev-util/cmake"
RDEPEND=""

S="${WORKDIR}"/osmo-sdr-hopscotch-${PV}/software/${PN}/

src_prepare() {
	eautoreconf
}

src_install() {
	default
	mv "${D}"/usr/share/doc/{${PN}/${PN}-UNKNOWN,${PF}}/html || die
	rmdir "${D}"/usr/share/doc/${PN}{/${PN}-UNKNOWN,} || die
}
