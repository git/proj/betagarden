# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit autotools

DESCRIPTION="C++ bindings of libcurl"
HOMEPAGE="http://www.curlpp.org/"
SRC_URI="https://github.com/jpbarrette/curlpp/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="net-misc/curl"
RDEPEND="${DEPEND}"

src_prepare() {
	sed -i -e "s:@CURLPP_CFLAGS@:@CURLPP_CXXFLAGS@:" curlpp-config.in || die
	epatch "${FILESDIR}/disable-examples-build.patch" #example18 doesnt build
	eautoreconf
}

src_install() {
	default
	dodoc doc/guide.pdf
	insinto /usr/share/${P}
	doins examples/*
}
