# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils git-r3 toolchain-funcs

DESCRIPTION="A small 'net top' tool, grouping bandwidth by process"
HOMEPAGE="https://github.com/raboof/nethogs"
SRC_URI=""
EGIT_REPO_URI="https://github.com/raboof/nethogs.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

RDEPEND="
	net-libs/libpcap
	sys-libs/ncurses:0=
"
DEPEND="
	${RDEPEND}
	virtual/pkgconfig
"

DOCS=( DESIGN README.decpcap.txt README.md )

src_compile() {
	tc-export CC CXX PKG_CONFIG
	emake NCURSES_LIBS="$($(tc-getPKG_CONFIG) --libs ncurses)" ${PN}
}

src_install() {
	emake DESTDIR="${ED}" prefix=/usr install
	dodoc ${DOCS[@]}
}
