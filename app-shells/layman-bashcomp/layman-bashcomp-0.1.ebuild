# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Bash completion for app-portage/layman"
HOMEPAGE="http://www.penguindevelopment.org/"
SRC_URI="http://proj.penguindevelopment.org/${PN}/${P}.tar.gz"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="app-shells/bash-completion"
