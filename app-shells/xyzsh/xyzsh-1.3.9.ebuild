# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit autotools-utils

DESCRIPTION="xyzsh shell script language"
HOMEPAGE="http://xyzsh.sourceforge.jp/"
SRC_URI="mirror://sourceforge.jp/xyzsh/58225/${P}.tgz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="migemo"

DEPEND="dev-libs/oniguruma
	migemo? ( app-text/cmigemo )"
RDEPEND="${DEPEND}"

PATCHES=(
	"${FILESDIR}"/${P}-soname.patch
	"${FILESDIR}"/${P}-cflags.patch
	"${FILESDIR}"/${P}-ldflags.patch
	"${FILESDIR}"/${P}-strip.patch
	"${FILESDIR}"/${P}-make.patch
	"${FILESDIR}"/${P}-gcc.patch
	"${FILESDIR}"/${P}-libdl.patch
	"${FILESDIR}"/${P}-tinfo.patch
	"${FILESDIR}"/${P}-configure.patch
)
AUTOTOOLS_IN_SOURCE_BUILD=1
AUTOTOOLS_AUTORECONF=1

src_configure() {
	local myeconfargs=(
		$(use_enable migemo)
	)
	autotools-utils_src_configure
}
