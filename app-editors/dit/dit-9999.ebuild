# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="ncurses-based text editor for Unix systems"
HOMEPAGE="http://dit-editor.sourceforge.net/"
EGIT_REPO_URI="https://github.com/hishamhm/dit.git"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="dev-lang/lua:5.3"
RDEPEND="${DEPEND}"

src_prepare() {
	eapply_user
	./autogen.sh || die
}
