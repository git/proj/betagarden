# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils gnome2-utils scons-utils subversion

DESCRIPTION="Real-time animation editor"
HOMEPAGE="http://animata.kibu.hu/index.html"
ESVN_REPO_URI="http://${PN}.googlecode.com/svn/trunk/"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="x11-libs/fltk"
DEPEND="${RDEPEND}"

src_compile() {
	escons
}

src_install() {
	insinto /usr/share/${PN}/
	doins -r examples || die

	newicon data/animata_icon.png ${PN}.png
	make_desktop_entry ${PN} ${PN^} ${PN} 'AudioVideo;Video'

	dobin build/${PN} || die
}

pkg_postinst() {
	gnome2_icon_cache_update
}

pkg_postrm() {
	gnome2_icon_cache_update
}
