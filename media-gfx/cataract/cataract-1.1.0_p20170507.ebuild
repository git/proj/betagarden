# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools

MY_COMMIT=29381f9e70d122ea4d89d71ea97f42253c528211
DESCRIPTION="Simple static web photo gallery, designed to be clean and easily usable."
HOMEPAGE="http://cgg.bzatek.net/"
SRC_URI="http://git.bzatek.net/cataract/snapshot/${MY_COMMIT}.tar.gz -> ${P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-libs/glib-2.36
	dev-libs/libxml2
	<media-gfx/imagemagick-7
	media-gfx/exiv2"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

S="${WORKDIR}"/${PN}-${MY_COMMIT:0:7}

DOCS=( AUTHORS NEWS README )

src_prepare() {
	eapply_user
	eautoreconf
}

src_install() {
	default

	# Install sample
	rm sample/{gen.sh,Makefile*} || die
	sed 's|<path>../../templates</path>|<path>/usr/share/cgg</path>|' \
			-i sample/src/setup.xml || die
	insinto /usr/share/cgg/
	doins -r sample || die
}
