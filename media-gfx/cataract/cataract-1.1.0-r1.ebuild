# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"

DESCRIPTION="Simple static web photo gallery, designed to be clean and easily usable."
HOMEPAGE="http://cgg.bzatek.net/"
SRC_URI="http://cgg.bzatek.net/files/${P}.tar.bz2"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-libs/glib
	dev-libs/libxml2
	<media-gfx/imagemagick-7
	media-gfx/exiv2"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc NEWS README AUTHORS || die

	# Install sample
	rm sample/{gen.sh,Makefile*,src.tar.gz} || die
	sed 's|<path>../../templates</path>|<path>/usr/share/cgg</path>|' \
			-i sample/src/setup.xml || die
	insinto /usr/share/cgg/
	doins -r sample || die
}
