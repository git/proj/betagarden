# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

inherit unpacker

MY_PN=${PN%%-bin}-mono
DESCRIPTION="3D CAD program"
HOMEPAGE="http://cad.arcad.de/products_architecture_arcad.php"
SRC_URI="http://packages.arcad.de/FREE-Linux/pool/precise/stable/a/${MY_PN}/${MY_PN}_${PV}_amd64.deb"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

# Inspired by from debian/control file
DEPEND=""
RDEPEND="!app-office/lxfibu-c1-bin
	>=sys-libs/glibc-2.14
	>=media-libs/fontconfig-2.8.0
	>=media-libs/freetype-2.2.1
	media-libs/mesa
	virtual/glu
	virtual/jpeg
	media-libs/libpng:1.2
	x11-libs/libX11
	x11-libs/libXext
	x11-libs/libXft
	dev-libs/libxml2
	x11-libs/libXmu
	x11-libs/libXp
	x11-libs/libXrender
	x11-libs/libXt"

S=${WORKDIR}
RESTRICT="mirror strip"

src_install() {
	local app=${PN%%-bin}
	local target=_Linux_x86_64_3.5.0

	cp -R . "${D}"/ || die
	dosym /opt/tuxbase/projects/${app}/${app}.desktop /usr/share/applications/${app}.desktop || die
	dosym lib${target} /opt/tuxbase/lib || die
	dosym bin${target} /opt/tuxbase/projects/${app}/bin || die
	dosym /opt/tuxbase/projects/${app}/start /usr/bin/${app} || die

	dosym /usr/lib64/libjpeg.so /opt/tuxbase/lib/libjpeg.so.62 || die

	# Ensure that the shipped libXm.so.4.0.3 is used rather than the system-wide libXm.so.4.0.4 or we get:
	# symbol lookup error: /opt/tuxbase/lib/libtxtbl.so.2010.02: undefined symbol: _XmXftSetClipRectangles
	dosym libXm.so.4.0.3 /opt/tuxbase/lib/libXm.so.4 || die
}
