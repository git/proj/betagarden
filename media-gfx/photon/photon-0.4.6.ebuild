# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

MY_P="${PN^}-${PV}"
DESCRIPTION="Static HTML gallery generator"
HOMEPAGE="http://www.saillard.org/programs_and_patches/photon/"
SRC_URI="http://www.saillard.org/programs_and_patches/${PN}/files/${MY_P}.tar.bz2"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="raw"

DEPEND=""
RDEPEND="
	dev-python/imaging[${PYTHON_USEDEP}]
	raw? ( media-gfx/dcraw )"

S="${WORKDIR}"/${MY_P}

PATCHES=( "${FILESDIR}"/${P}-pil.patch )
