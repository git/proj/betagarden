# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils

DESCRIPTION="Tools for extracting information from Gimp XCF files"
HOMEPAGE="https://github.com/j-jorge/xcftools/"
SRC_URI="http://henning.makholm.net/${PN}/${P}.tar.gz
	mirror://debian/pool/main/${PN:0:1}/${PN}/${PN}_${PV}-6.debian.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

CDEPEND="media-libs/libpng:*"
DEPEND="${CDEPEND}
	dev-lang/perl"
RDEPEND="${CDEPEND}
	x11-misc/xdg-utils"

DOCS=( ChangeLog README )

src_prepare() {
	rm "${WORKDIR}"/debian/patches/series || die
	epatch "${WORKDIR}"/debian/patches/*
	eapply_user
}
