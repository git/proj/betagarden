# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

VALA_MIN_API_VERSION=0.16
inherit toolchain-funcs multilib vala

DESCRIPTION="Free font editor"
HOMEPAGE="http://birdfont.org/"
SRC_URI="http://birdfont.org/releases/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gtk"

DEPEND="
	dev-lang/python
	dev-python/doit
	$(vala_depend)
	"
RDEPEND="
	gtk? (
		x11-libs/cairo
		x11-libs/gtk+:3
		net-libs/webkit-gtk:3
		net-libs/libsoup:2.4
		x11-libs/libnotify
	)
	x11-libs/gdk-pixbuf:2
	dev-libs/glib:2
	dev-libs/libxml2:2
	dev-libs/libgee:0.8
	"

src_prepare() {
	local valac="vala-$(vala_best_api_version)"
	sed -i 's|"valac"|"'"${valac}"'"|' configure || die
}

src_configure() {
	# Python-based configure!
	use gtk || local use_gtk='--gtk False'
	./configure \
			--prefix /usr \
			--dest "${D}" \
			--cc "$(tc-getCC)" \
			${use_gtk} \
			|| die
}

src_compile() {
	local valac="valac-$(vala_best_api_version)"
	./scripts/linux_build.py \
			--prefix /usr \
			--dest "${D}" \
			--cc "$(tc-getCC)" \
			--cflags "${CFLAGS}" \
			--ldflags "${LDFLAGS}" \
			--valac "${valac}" \
			|| die
}

src_install() {
	./install.py \
			--dest "${D}" \
			--nogzip True \
			--manpages-directory "/share/man/man1" \
			--libdir "${get_libdir}" \
			|| die
}
