# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

DESCRIPTION="Like jpegtran plus in-depth handling of EXIF data"
HOMEPAGE="http://www.kraxel.org/blog/linux/fbida/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="alpha amd64 arm hppa ppc ppc64 sh sparc x86"
IUSE=""

DEPEND=""
RDEPEND=">=media-gfx/fbida-2"
