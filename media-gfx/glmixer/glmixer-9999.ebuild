# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"
CMAKE_IN_SOURCE_BUILD=yes

inherit eutils subversion cmake-utils

DESCRIPTION="Real-time graphics mixing for video performance"
HOMEPAGE="https://code.google.com/p/glmixer/"
ESVN_REPO_URI="https://${PN}.googlecode.com/svn/trunk/"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="
	dev-qt/qtcore
	dev-qt/qtgui
	dev-qt/qtopengl
	dev-qt/qtsvg
	dev-qt/qtwebkit
	media-libs/glew
	media-libs/glu
	media-libs/opencv
	virtual/ffmpeg"
DEPEND="${RDEPEND}"

src_prepare() {
	epatch "${FILESDIR}"/${P}-subversion.patch
	rm -Rf libFreeFrameGL/FFGL-SDK-1.5 || die
	ewarn 'Libraries still bundled: libQtProperty, libOSCPack, libFreeFrameGL 1.6'
}

src_configure() {
	local mycmakeargs=(
		-DUSE_SHAREDMEMORY=yes
		-DUSE_OPENCV=yes
		-DUSE_FREEFRAMEGL=1.6
		-Wno-dev
	)

	cmake-utils_src_configure
}
