# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python{2_7,3_{4,5,6}} )

inherit distutils-r1

DESCRIPTION="Static web gallery generator"
HOMEPAGE="https://sml.zincube.net/~niol/repositories.git/lazygal/about/"
SRC_URI="https://sml.zincube.net/~niol/repositories.git/lazygal/snapshot/${P}.tar.bz2"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	>=dev-python/pillow-1.1.6["${PYTHON_USEDEP}"]
	dev-python/pyexiv2["${PYTHON_USEDEP}"]
	>=dev-python/genshi-0.5["${PYTHON_USEDEP}"]
	sys-devel/gettext"
DEPEND="${RDEPEND}
	dev-libs/libxslt"
