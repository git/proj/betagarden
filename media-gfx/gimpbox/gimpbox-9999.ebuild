# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit mercurial python-r1

DESCRIPTION="GIMP single window mode"
HOMEPAGE="http://code.google.com/p/gimpbox/"
SRC_URI=""
EHG_REPO_URI="http://gimpbox.googlecode.com/hg/"

LICENSE="LGPL-2 LGPL-3"  # LGPL version notclearly stated upstream
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="
	dev-python/libwnck-python
	${PYTHON_DEPS}
	"
RDEPEND="${DEPEND}
	media-gfx/gimp"

S=${WORKDIR}/hg

src_install() {
	python_foreach_impl python_newscript gimpbox.py gimpbox
}
