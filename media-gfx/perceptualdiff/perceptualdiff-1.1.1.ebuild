# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit cmake-utils

MY_PN=pdiff
DESCRIPTION="Image comparison, that makes use of a computational model of the human visual system"
HOMEPAGE="http://pdiff.sourceforge.net/"
SRC_URI="mirror://sourceforge/project/${MY_PN}/${MY_PN}/${P}/${P}-src.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="media-libs/freeimage"
RDEPEND="${DEPEND}"

S=${WORKDIR}

DOCS=( README.txt )

PATCHES=( "${FILESDIR}"/${P}-compile.patch )
