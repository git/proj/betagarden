# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python2_7 )

inherit git-r3 autotools python-single-r1 toolchain-funcs

DESCRIPTION="Traces TeX fonts to PFA or PFB fonts (formerly pktrace)"
HOMEPAGE="http://lilypond.org/mftrace/"
EGIT_REPO_URI="https://github.com/hanwen/mftrace.git"
LICENSE="GPL-2"
KEYWORDS=""
# SLOT 1 was used in pktrace ebuild
SLOT="1"
IUSE="doc test truetype"

RDEPEND=">=app-text/t1utils-1.25
	|| ( media-gfx/potrace >=media-gfx/autotrace-0.30 )
	truetype? ( media-gfx/fontforge )
	virtual/latex-base
	${PYTHON_DEPS}"
DEPEND="${RDEPEND}
	doc? ( sys-apps/texinfo )
	test? ( media-gfx/fontforge )"

src_prepare() {
	eapply_user
	eautoreconf
}

src_configure() {
	tc-export CC
	econf \
		--datadir="$(python_get_sitedir)"
}

src_compile() {
	emake CFLAGS="-Wall ${CFLAGS}"
	use doc && emake README.txt
}

src_install () {
	default
	python_optimize
	dodoc ChangeLog
	use doc && dodoc README.txt
}
