# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit versionator eutils

DEBIAN_LEVEL=${PF##*-r}
DESCRIPTION="Transform DocBook SGML into nroff/troff man pages"
HOMEPAGE="http://www.oasis-open.org/docbook/tools/dtm/"
SRC_URI="mirror://debian/pool/main/${PN:0:1}/${PN}/${PN}_${PV}.orig.tar.gz
	mirror://debian/pool/main/${PN:0:1}/${PN}/${PN}_${PV}-${DEBIAN_LEVEL}.debian.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="app-text/openjade
	app-text/docbook-xml-dtd:4.5
	app-text/docbook-sgml-dtd:4.5"

S=${WORKDIR}/${PN}-${PV}.orig

src_prepare() {
	epatch "${WORKDIR}"/debian/patches/*.patch
	epatch "${FILESDIR}"/${P}-compile.patch
	epatch "${FILESDIR}"/${P}-paths.patch
	epatch "${FILESDIR}"/${P}-docbook-4.5.patch
}

src_compile() {
	emake -C Instant dotptregexp || die
	emake all || die
}

src_install() {
	dodir /usr/{bin,lib,share/sgml} || die
	emake ROOT="${D}"/usr install || die
	emake ROOT="${D}"/usr -C Instant/tptregexp install || die
	doman Doc/*.1 || die
}
