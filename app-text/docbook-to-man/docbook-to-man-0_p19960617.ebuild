# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit eutils

DESCRIPTION="Transform DocBook SGML into nroff/troff man pages"
HOMEPAGE="http://www.oasis-open.org/docbook/tools/dtm/"
SRC_URI="http://www.oasis-open.org/docbook/tools/dtm/${PN}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""  # masked for now, does not yet work as expected
IUSE=""

DEPEND=""
RDEPEND=""

S=${WORKDIR}/${PN}

src_prepare() {
	epatch "${FILESDIR}"/${P}-compile.patch
}

src_compile() {
	emake -C Instant dotptregexp || die
	emake all || die
}

src_install() {
	dodir /usr/{bin,lib} || die
	emake ROOT="${D}"/usr install || die
	emake ROOT="${D}"/usr -C Instant/tptregexp install || die
	doman Doc/*.1 || die
}
