# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

MY_P=${P/-xml-dtd/}

DESCRIPTION="Docbook DTD for XML"
HOMEPAGE="http://www.docbook.org/"
SRC_URI="http://www.docbook.org/xml/${PV}/${MY_P}.zip"

LICENSE="docbook"
SLOT="${PV}"
KEYWORDS=""  # Lazy mask
IUSE=""

RDEPEND=""
DEPEND=">=app-arch/unzip-5.41"

S=${WORKDIR}/${MY_P}

src_install() {
	keepdir /etc/xml

	insinto /usr/share/sgml/docbook/xml-dtd-${PV}
	doins dtd/*.dtd catalog.xml || die

	dodoc ChangeLog README*
}
