# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit python-r1

DESCRIPTION="Wrapper around manServer (app-text/manserver)"
HOMEPAGE="http://git.goodpoint.de/?p=man2tidyhtml.git;a=summary"
SRC_URI="http://www.hartwork.org/public/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	|| (
		=app-text/manserver-1.07
		=app-text/manserver-1.08 )
	dev-python/utidylib[${PYTHON_USEDEP}]"
DEPEND=""

S="${WORKDIR}/${PN}"

src_install() {
	dodoc AUTHORS
	python_foreach_impl python_doscript ${PN}
}
