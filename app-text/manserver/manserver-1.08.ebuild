# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="man pages to HTML converter and conversion server"
HOMEPAGE="http://www.squarebox.co.uk/users/rolf/download/manServer.shtml"
SRC_URI="http://www.squarebox.co.uk/download/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-lang/perl"
DEPEND=""

MY_PN="manServer"

S="${WORKDIR}/${MY_PN}"

src_install() {
	dodoc AUTHORS
	doman ${MY_PN}.1
	newbin ${MY_PN}.pl ${MY_PN}
}
