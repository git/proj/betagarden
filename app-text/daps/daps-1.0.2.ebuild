# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

DESCRIPTION="DocBook Authoring and Publishing Suite"
HOMEPAGE="http://daps.sourceforge.net/"
SRC_URI="mirror://sourceforge/project/${PN}/${P}.tar.bz2"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-apps/sed"
RDEPEND="media-gfx/imagemagick
	app-text/docbook-xml-dtd
	app-text/docbook-xsl-stylesheets
	dev-libs/libxml2
	dev-libs/libxslt"  # TODO see configure output

S=${WORKDIR}/${PN}

src_install() {
	# Workaround a sandbox issue
	local root_catalog="${D}"/etc/xml/catalog.${PN}.unused
	dodir /etc/xml || die
	cp /etc/xml/catalog "${root_catalog}" || die
	emake DESTDIR="${D}" ROOT_CATALOG="${root_catalog}" install || die
}

pkg_postinst() {
	ewarn "You may want to inspect the output of"
	ewarn ""
	ewarn "  # diff -u /etc/xml/catalog{,.${PN}.unused}"
	ewarn ""
	ewarn "and merge/copy the changes"
}
