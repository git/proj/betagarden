# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="Task management GUI tool"
HOMEPAGE="http://nitrotasks.com/"
SRC_URI="https://launchpad.net/nitrotasks/trunk/${PV}/+download/${PN}_${PV}.tar.gz"

LICENSE="BSD-4"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-python/python-distutils-extra[${PYTHON_USEDEP}]"
RDEPEND="dev-python/pywebkitgtk[${PYTHON_USEDEP}]"

S="${WORKDIR}"/${PN}
