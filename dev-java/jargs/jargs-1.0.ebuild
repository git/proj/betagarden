# Copyright 2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

JAVA_PKG_IUSE="doc"
inherit java-pkg-2 java-ant-2

DESCRIPTION="JArgs command line option parsing suite for Java"
HOMEPAGE="http://jargs.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.bz2"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=virtual/jdk-1.3"
RDEPEND=">=virtual/jre-1.3"

EANT_BUILD_TARGET="compile runtimejar"
EANT_DOC_TARGET="javadoc"

java_prepare() {
	rm -v lib/*.jar || die
}

src_install() {
	java-pkg_dojar "lib/${PN}.jar"
}
