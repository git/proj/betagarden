# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

USE_RUBY=( ruby19 )
inherit eutils ruby-ng

DESCRIPTION="Ruby bindings to ncurses"
HOMEPAGE="http://sourceforge.net/projects/ncurses-ruby.berlios/"
SRC_URI="mirror://sourceforge/project/${PN}.berlios/${P}.tar.bz2"

LICENSE="LGPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}"/all/${P}

src_prepare() {
	# https://bugzilla.redhat.com/show_bug.cgi?id=822814#c1
	epatch "${FILESDIR}"/${P}-ruby19.patch
}

src_compile() {
	local ruby=$(ruby_implementation_command ruby19)
	${ruby} extconf.rb

	emake
}

src_install() {
	emake DESTDIR="${D}" install
}
