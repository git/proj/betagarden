# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils cmake-utils git-r3

DESCRIPTION="Free alternative for Fruityloops, Cubase and Logic"
HOMEPAGE="http://lmms.sourceforge.net/"
EGIT_REPO_URI="git://lmms.git.sf.net/gitroot/${PN}/${PN}"

LICENSE="GPL-2 LGPL-2"
SLOT="0"
KEYWORDS=""
IUSE="alsa debug fftw fluidsynth jack ogg portaudio pulseaudio sdl stk vst"

RDEPEND="
	dev-qt/qtcore:4
	dev-qt/qtgui:4[accessibility]
	>=media-libs/libsndfile-1.0.11
	>=media-libs/libsamplerate-0.1.7
	x11-libs/libXinerama
	alsa? ( media-libs/alsa-lib )
	fftw? ( sci-libs/fftw:3.0 >=x11-libs/fltk-1.3.0_rc3:1 )
	jack? ( >=media-sound/jack-audio-connection-kit-0.99.0 )
	ogg? (
		media-libs/libvorbis
		media-libs/libogg
	)
	fluidsynth? ( media-sound/fluidsynth )
	portaudio? ( >=media-libs/portaudio-19_pre )
	pulseaudio? ( media-sound/pulseaudio )
	sdl? (
		media-libs/libsdl
		>=media-libs/sdl-sound-1.0.1
	)
	stk? ( media-libs/stk )
	vst? ( app-emulation/wine )"
DEPEND="${RDEPEND}
	>=dev-util/cmake-2.4.5"
RDEPEND="${RDEPEND}
	media-plugins/swh-plugins
	media-plugins/caps-plugins
	media-plugins/tap-plugins
	media-libs/ladspa-cmt"

DOCS="README AUTHORS TODO"

PATCHES=( "${FILESDIR}/${PN}-0.4.10-unembedfltk.patch" )

src_configure() {
	local mycmakeargs=(
		-DWANT_SYSTEM_SR=TRUE
		-DWANT_CAPS=FALSE
		-DWANT_TAP=FALSE
		-DWANT_SWH=FALSE
		-DWANT_CMT=FALSE
		-DWANT_CALF=TRUE
		-DWANT_ALSA=$(usex alsa)
		-DWANT_FFTW3F=$(usex fftw)
		-DWANT_JACK=$(usex jack)
		-DWANT_OGGVORBIS=$(usex ogg)
		-DWANT_PORTAUDIO=$(usex portaudio)
		-DWANT_PULSEAUDIO=$(usex pulseaudio)
		-DWANT_SDL=$(usex sdl)
		-DWANT_STK=$(usex stk)
		-DWANT_VST=$(usex vst)
		-DWANT_SF2=$(usex fluidsynth)
	)
	cmake-utils_src_configure
}
