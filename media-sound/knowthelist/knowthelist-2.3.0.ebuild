# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit qmake-utils

DESCRIPTION="Party music player"
HOMEPAGE="http://knowthelist.github.io/knowthelist/"
SRC_URI="https://github.com/knowthelist/knowthelist/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-libs/glib:2

	media-libs/gstreamer:1.0
	media-libs/taglib
	media-libs/alsa-lib

	dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtsql:5
	dev-qt/qtwidgets:5
	dev-qt/qtxml:5
	"
RDEPEND="${DEPEND}"

DOCS=( README.md )

src_configure() {
	eqmake5
}

src_install() {
	dodoc "${DOCS[@]}"
	emake INSTALL_ROOT="${D}" install
}
