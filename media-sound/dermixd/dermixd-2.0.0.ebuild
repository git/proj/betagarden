# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

CONFIG_PARAM_VERSION=3.000010
DESCRIPTION="Multi-stream music player daemon"
HOMEPAGE="http://thomas.orgis.org/dermixd/i.shtml"
SRC_URI="http://thomas.orgis.org/${PN}/${P}.tar.bz2
	mirror://cpan/authors/id/T/TH/THORGIS/Config-Param-${CONFIG_PARAM_VERSION}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-lang/perl
	media-libs/alsa-lib
	media-libs/libsndfile
	media-libs/libvorbis
	media-sound/mpg123
	sys-apps/file
	sys-devel/gcc[cxx]
	"
RDEPEND="${DEPEND}"

src_configure() {
	# non-autoconf configure script
	# SoundTouch disabled due to error "I need SoundTouch with 32 bit floating point numbers"
	./configure \
			--disable-debug \
			--disable-memdebug \
			--disable-mme \
			--disable-oss \
			--disable-soundtouch \
			--disable-xdebug \
			--prefix=/usr \
			|| die
}

src_install() {
	default

	# TODO: Extract dedicated package, fix paths if needed
	insinto /usr/lib/dermixd/
	doins -r ../Config-Param-${CONFIG_PARAM_VERSION}/lib/Config
}
