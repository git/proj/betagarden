# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils

DESCRIPTION="Multi-stream music player daemon"
HOMEPAGE="http://thomas.orgis.org/dermixd/i.shtml"
SRC_URI="http://thomas.orgis.org/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-lang/perl
	media-libs/alsa-lib
	media-libs/libsndfile
	media-libs/libvorbis
	media-sound/mpg123
	sys-devel/gcc[cxx]
	"
RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${FILESDIR}"/${P}-compile.patch
}

src_compile() {
	emake SNDFILE=yes VORBISFILE=yes gnu-alsa || die
}

src_install() {
	# Library
	insinto /usr/lib/${PN}
	doins frontend/Param.pm
	doins -r frontend/DerMixD

	# Commands
	newbin frontend/simple_player ${PN}-simple-player
	newbin frontend/shuffle ${PN}-shuffle
	dobin frontend/${PN}-control
	dobin ${PN}
}
