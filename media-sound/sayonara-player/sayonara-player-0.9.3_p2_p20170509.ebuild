# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

CMAKE_BUILD_TYPE=None
inherit eutils cmake-utils

MY_PV=${PV/_p/-git}; MY_PV=${MY_PV/_p/-}
DESCRIPTION="Small, clear and fast audio player for Linux"
HOMEPAGE="https://sayonara-player.com/"
SRC_URI="https://sayonara-player.com/sw/${PN}-${MY_PV}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

MIN_QT_VERSION=5.3

RDEPEND="
	dev-libs/glib:2
	>=dev-qt/qtcore-${MIN_QT_VERSION}:5
	>=dev-qt/qtdbus-${MIN_QT_VERSION}:5
	>=dev-qt/qtgui-${MIN_QT_VERSION}:5
	>=dev-qt/qtnetwork-${MIN_QT_VERSION}:5
	>=dev-qt/qtsql-${MIN_QT_VERSION}:5[sqlite]
	>=dev-qt/qtwidgets-${MIN_QT_VERSION}:5
	>=dev-qt/qtxml-${MIN_QT_VERSION}:5
	media-libs/libmtp
	>=media-libs/taglib-1.6
	media-libs/gst-plugins-base:1.0
	media-libs/gstreamer:1.0
	media-plugins/gst-plugins-soundtouch:1.0
	sys-libs/zlib
	"
DEPEND="${RDEPEND}
	virtual/pkgconfig
	"

S="${WORKDIR}"/${PN}

src_prepare() {
	epatch "${FILESDIR}"/${PN}-0.9.2_p4_p20160920-updates.patch
	eapply_user
}

pkg_postinst() {
	elog "Optionally, install as well:"
	elog "  media-sound/lame"
	elog "  media-libs/gst-plugins-good:1.0"
	elog "  media-libs/gst-plugins-bad:1.0"
}
