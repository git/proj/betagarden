# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="alsamixer alike for PulseAudio"
HOMEPAGE="https://github.com/KenjiTakahashi/pacmixer/"
SRC_URI="https://github.com/KenjiTakahashi/pacmixer/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+ MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

CDEPEND="gnustep-base/gnustep-base
	media-sound/pulseaudio
	sys-libs/ncurses"
DEPEND="${CDEPEND}
	dev-util/ninja
	sys-devel/gcc[cxx,objc,objc++]"
RDEPEND="${CDEPEND}
	sys-devel/gcc[cxx]"

src_compile() {
	./mk || die
}

src_install() {
	DESTDIR="${D}" PREFIX=/usr ./mk install || die
}
