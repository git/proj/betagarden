# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

PYTHON_COMPAT=( python2_7 )
inherit distutils-r1

MY_PV=99cea2fe72620076e7f10d1f2735cf83cf5055c5
DESCRIPTION=" alsamixer-like curses interface for pulseaudio"
HOMEPAGE="https://github.com/Valodim/pamixer"
SRC_URI="https://github.com/Valodim/pamixer/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-python/libpulseaudio"

S="${WORKDIR}"/${PN}-${MY_PV}

src_prepare() {
	epatch "${FILESDIR}"/${P}-install-pamixer-pulse-module.patch
}
