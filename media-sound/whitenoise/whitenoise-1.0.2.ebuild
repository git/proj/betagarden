# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit eutils

DESCRIPTION="Small utility which turns your computer into an ambient random noise generator."
HOMEPAGE="http://pessimization.com/software/whitenoise/"
SRC_URI="http://pessimization.com/software/${PN}/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc fftw"

DEPEND="fftw? ( sci-libs/fftw )"
RDEPEND="${DEPEND}"

src_configure() {
	econf $(use_enable fftw) \
		--disable-arts
}

src_install() {
	dobin whitenoise || die
	if use doc ; then
		dodoc doc/manual.html doc/HACKING || die
	fi
}
