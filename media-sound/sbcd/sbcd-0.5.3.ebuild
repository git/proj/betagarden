# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit eutils autotools

DESCRIPTION="Curses CD player based on the interface of a tool shipped with SoundBlaster 16."
HOMEPAGE="http://sbcd.sourceforge.net/"
SRC_URI="mirror://sourceforge/project/${PN}/${PN}/${PN}%20${PV}/${P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug"

DEPEND="media-libs/libsdl"
RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${FILESDIR}"/${P}-configure.patch
	eautoconf
	eautomake
}

src_configure() {
	econf $(use debug && echo " --enable-freedbdebug")
}

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc BUGS ChangeLog AUTHORS README TODO || die
}
