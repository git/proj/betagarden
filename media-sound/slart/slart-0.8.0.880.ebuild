# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit versionator eutils

MY_PV="$(get_version_component_range 1-3)-$(get_version_component_range 4)"

DESCRIPTION="Collection of audio tools (SvOlli's Little Audio Related Thingies)"
HOMEPAGE="http://svolli.org/software/slart/"
SRC_URI="http://svolli.org/download.php/software/slart/${PN}-${MY_PV}.tar.bz2"

LICENSE="GPL-3 LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	app-arch/bzip2
	app-misc/lirc
	dev-qt/qtcore:4
	dev-qt/qtgui:4
	dev-qt/qtsql:4
	media-libs/flac
	media-libs/libogg
	media-libs/libvorbis
	media-sound/lame
	"
RDEPEND="${DEPEND}
	<media-sound/dermixd-2"

S="${WORKDIR}"/${PN}-${MY_PV}

src_prepare() {
	epatch "${FILESDIR}"/${P}-*.patch
}

src_install() {
	default
	rm "${ED}"/usr/share/applications/slart-stripped.desktop
}
