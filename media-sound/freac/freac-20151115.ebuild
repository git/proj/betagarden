# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Audio converter and CD ripper"
HOMEPAGE="https://www.freac.org/"
SRC_URI="https://freac.org/preview/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	media-libs/freac-cdk
	sys-devel/gcc:*[cxx]
	x11-libs/smooth
	"
DEPEND="${RDEPEND}"

src_compile() {
	emake prefix=/usr
}

src_install() {
	emake DESTDIR="${D}" prefix=/usr install
}
