# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Chess and Xiangqi engines"
HOMEPAGE="http://www.hoicher.de/hoichess/download/"
SRC_URI="http://www.hoicher.de/${PN}/download/${P}-src.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-lang/perl"
RDEPEND=""

S="${WORKDIR}"/${P}-src

src_prepare() {
	cat <<-MAKEFILE_CONFIG > src/Makefile.config
		CXXFLAGS += ${CXXFLAGS}

		HAVE_READLINE = 1
		HAVE_PTHREAD = 1
	MAKEFILE_CONFIG
}

src_install() {
	default
	
	dodir /usr/games/bin
	mv "${D}"/usr/games/{hoichess,hoixiangqi} "${D}"/usr/games/bin || die
}
