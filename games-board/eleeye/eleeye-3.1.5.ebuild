# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit versionator games

MY_PV="$(get_version_component_range 1-2)$(get_version_component_range 3)"
DESCRIPTION="Chinese chess engine (UCCI protocol)"
HOMEPAGE="https://sourceforge.net/projects/xqwizard/"
SRC_URI="mirror://sourceforge/project/xqwizard/4.%20ElephantEye/4-1.%20ElephantEye%20${MY_PV}%20%28UCCI%20Engine%29%20Source/${PN}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

S="${WORKDIR}"

src_prepare() {
	cp "${FILESDIR}"/${P}-makefile ${PN}/Makefile || die
}

src_compile() {
	emake -C ${PN} || die
}

src_install() {
	dogamesbin ${PN}/${PN} || die
	dolib ${PN}/evaluate/libeval.so || die
}
