# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit subversion games

DESCRIPTION="Chinese chess engine (UCCI protocol)"
HOMEPAGE="https://sourceforge.net/projects/xqwizard/"
ESVN_REPO_URI="svn://svn.code.sf.net/p/xqwizard/code/ELEEYE"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND=""

S="${WORKDIR}"

src_unpack() {
	subversion_src_unpack
}

src_prepare() {
	subversion_src_prepare
	cp "${FILESDIR}"/${P}-makefile ${PN}/Makefile || die
}

src_compile() {
	emake -C ${PN} || die
}

src_install() {
	dogamesbin ${PN}/${PN} || die
}
