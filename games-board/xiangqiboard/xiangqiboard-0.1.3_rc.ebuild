# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils

MY_P=${PN}-${PV/_/-}
DESCRIPTION="Scalable Xiangqi GUI"
HOMEPAGE="http://xiangqiboard.blogspot.de/"
SRC_URI="http://ralph-glass.homepage.t-online.de/xiangqi/${MY_P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

SHARED_DEPEND="dev-haskell/gtk
	dev-haskell/cairo"

RDEPEND="${SHARED_DEPEND}
	games-board/hoichess"
DEPEND="${SHARED_DEPEND}
	dev-lang/ghc"

S="${WORKDIR}"/${MY_P}

src_prepare() {
	epatch "${FILESDIR}"/${MY_P}-modern-ghc.patch
	epatch "${FILESDIR}"/${MY_P}-paths.patch
}

src_install() {
	dodir /usr/games/bin || die
	emake prefix="${D}/usr" install || die
	mv "${D}"/usr/games/${PN} "${D}"/usr/games/bin/ || die
}
