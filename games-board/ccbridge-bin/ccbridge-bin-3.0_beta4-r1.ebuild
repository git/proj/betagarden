# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils gnome2-utils

DESCRIPTION="Chinese chess software for Windows"
HOMEPAGE=""
SRC_URI="http://www.mediafire.com/download/lnmgnj4zhdt/CCBridge3beta4.zip"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/unzip
	media-gfx/icoutils"
RDEPEND="app-emulation/wineloc"

S="${WORKDIR}"
RESTRICT="mirror strip fetch"

src_unpack() {
	:
}

src_compile() {
	:
}

src_install() {
	local dir=/opt/${PN%-bin}
	local icon_name=${PN%-bin}

	dodir ${dir}
	( cd "${D}"/${dir} && unzip "${DISTDIR}/${A}" ) || die

	# Resolve pointless "CCBridge3" directory level
	mv "${D}${dir}"/CCBridge3/* "${D}${dir}"/ || die
	rmdir "${D}${dir}"/CCBridge3/ || die

	# Extract icon
	wrestool -x --name=MAINICON "${D}${dir}"/CCBridge.exe > ${icon_name}.ico
	icotool -x ${icon_name}.ico
	newicon --theme locolor --size 16 ${icon_name}_2_16x16x8.png ${icon_name}.png
	newicon --theme locolor --size 32 ${icon_name}_1_32x32x8.png ${icon_name}.png
	newicon --theme hicolor --size 16 ${icon_name}_6_16x16x32.png ${icon_name}.png
	newicon --theme hicolor --size 32 ${icon_name}_5_32x32x32.png ${icon_name}.png
	newicon --theme hicolor --size 48 ${icon_name}_4_48x48x32.png ${icon_name}.png
	newicon --theme hicolor --size 128 ${icon_name}_3_128x128x32.png ${icon_name}.png

	make_desktop_entry "wineloc -l zh_CN ${dir}/CCBridge.exe" 'CCBridge' ${icon_name} 'Game'
	mv "${D}"/usr/share/applications/{*,${PN}}.desktop || die
}

pkg_preinst() {
	gnome2_icon_savelist
}

pkg_postinst() {
	gnome2_icon_cache_update
}

pkg_postrm() {
	gnome2_icon_cache_update
}
