# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

MY_PN="GSquares"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="GTK-Based Strategy Game"
HOMEPAGE="http://dibyendu.github.com/GSquares/"
SRC_URI="mirror://sourceforge/project/gsquare/${MY_PN}/Version%20${PV}/${MY_P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="x11-libs/gtk+:2
	dev-libs/glib:2
	x11-libs/cairo"
DEPEND="${RDEPEND}
	sys-apps/sed"

S="${WORKDIR}"/${MY_P}

src_prepare() {
	sed -e "s|/share/|/share/${MY_PN}/|g" \
			-i "${S}"/src/paths.h.in || die
}

src_configure() {
	econf --datadir=/usr/share/${MY_PN}
}

src_install() {
	emake DESTDIR="${D}" install || die

	# Fix icon location
	sed 's|Icon=gsquares.png|Icon=/usr/share/GSquares/pixmaps/icon.png|' \
			-i "${D}"/usr/share/${MY_PN}/games/${MY_PN}.desktop || die

	# Move .desktop file in place
	dodir /usr/share/applications || die
	mv "${D}"/usr/share/{${MY_PN}/games/${MY_PN}.desktop,applications} || die
	rmdir "${D}"/usr/share/${MY_PN}/games || die
}
