# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Fairy-Max (user-defined chess engine) + MaxQi (Xiangqi engine)"
HOMEPAGE="http://home.hccnet.nl/h.g.muller/CVfairy.html"
SRC_URI="http://hgm.nubati.net/cgi-bin/gitweb.cgi?p=fairymax.git;a=snapshot;h=7d6f28cd573735ee96596a197d3f16694ddabb76;sf=tgz -> ${P}.tar.gz"

LICENSE="fairymax"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-lang/perl"
RDEPEND=""

S="${WORKDIR}"/fairymax-7d6f28c

src_install() {
	dodir /usr/games/bin
	default
	mv "${D}"/usr/games/*max* "${D}"/usr/games/bin/ || die

	dosym fairymax.6.bz2 /usr/share/man/man6/maxqi.6.bz2
	dosym fairymax.6.bz2 /usr/share/man/man6/shamax.6.bz2
}
