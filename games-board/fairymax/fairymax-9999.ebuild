# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit git-r3

DESCRIPTION="Fairy-Max (user-defined chess engine) + MaxQi (Xiangqi engine)"
HOMEPAGE="http://home.hccnet.nl/h.g.muller/CVfairy.html"
EGIT_REPO_URI="http://hgm.nubati.net/git/fairymax.git"

LICENSE="fairymax"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="dev-lang/perl"
RDEPEND=""

src_install() {
	dodir /usr/games/bin
	default
	mv "${D}"/usr/games/*max* "${D}"/usr/games/bin/ || die

	dosym fairymax.6.bz2 /usr/share/man/man6/maxqi.6.bz2
	dosym fairymax.6.bz2 /usr/share/man/man6/shamax.6.bz2
}
