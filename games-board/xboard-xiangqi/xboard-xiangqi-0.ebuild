# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit games

DESCRIPTION="Direct access to Xiangqi mode of xboard"
HOMEPAGE=""
SRC_URI=""

LICENSE="CC0-1.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="
	>=games-board/xboard-4.8
	games-board/hoichess
	games-board/fairymax
	"

S="${WORKDIR}"

src_install() {
	dosym xqboard.png "${GAMES_DATADIR}"/xboard/themes/textures/xqboard-9x10.png
	newgamesbin "${FILESDIR}"/xboard-xiangqi-0 xboard-xiangqi
}
