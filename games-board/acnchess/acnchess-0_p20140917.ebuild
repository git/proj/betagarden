# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils cmake-utils

DESCRIPTION="GTK+ 2.x Xiangqi GUI + engine 'acnguy'"
HOMEPAGE="https://github.com/naihe2010/acnchess"
SRC_URI="http://www.hartwork.org/public/${P}.tar.xz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	x11-libs/gtk+:2
	dev-libs/glib:2
	dev-libs/libttdht
	dev-libs/libecco-bin"
DEPEND="${RDEPEND}
	app-arch/xz-utils"

src_prepare() {
	# TODO rm -R libs/iniparser3.0b || die
	rm -R libs/openssl-0.9.8k || die
	rm -R libs/ecco || die
	rmdir libs/libttdht || die

	epatch "${FILESDIR}"/${P}-bundle.patch
}
