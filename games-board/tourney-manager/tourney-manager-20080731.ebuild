# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit perl-module

MY_P="${PN/-/_}-${PV}"
DESCRIPTION="Run chess engine tournaments"
HOMEPAGE="http://www.hoicher.de/hoichess/download/"
SRC_URI="http://www.hoicher.de/hoichess/download/${P}.tar.gz
	mirror://debian/pool/main/${PN:0:1}/${PN}/${PN}_20070820-4.diff.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-lang/perl[ithreads]"
RDEPEND="${DEPEND}
	dev-perl/yaml"

S="${WORKDIR}"/${MY_P}

src_unpack() {
	default
	filterdiff -i '*.6' < "${WORKDIR}"/tourney-manager_20070820-4.diff | patch -p 0 || die
	mv tourney-manager-20070820/debian/*.6 ${S}/ || die
}

src_compile() {
	:
}

src_install() {
	perl_set_version  # sets ${VENDOR_LIB}

	insinto "${VENDOR_LIB}"
	doins *.pm || die

	insinto /usr/share/doc/${PN}
	doins *.conf || die

	dodoc ChangeLog README TODO || die

	doman *.6 || die

	for i in crosstable engine-engine-match tourney ; do
		newbin ${i}.pl ${i} || die
	done
}
