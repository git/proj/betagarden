# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils games

MY_PN=xiangqi
DESCRIPTION="SDL-base Chinese chess program"
HOMEPAGE="https://www.iit.bme.hu/~salvi/archive/index.html"
SRC_URI="https://www.iit.bme.hu/~salvi/archive/${MY_PN}/${MY_PN}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-scheme/guile
	media-libs/libsdl
	media-libs/sdl-image
	"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_prepare() {
	epatch "${FILESDIR}"/${P}-chdir.patch
}

src_install() {
	insinto "${GAMES_DATADIR}"/${PN}
	doins -r data scripts
	make_desktop_entry ${PN} 'XiangQi (SDL, Peter Salvi)' '' 'Game'
	newgamesbin xiangqi ${PN}
}
