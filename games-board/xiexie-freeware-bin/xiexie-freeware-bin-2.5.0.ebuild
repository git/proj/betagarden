# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit desktop xdg

MY_PV=$(ver_cut 1-2)
DESCRIPTION="Free of cost Chinese chess software for Windows"
HOMEPAGE="http://xiexiemaster.com/"
SRC_URI="http://xiexiemaster.com/files/XXFreeware${MY_PV}.XInstaller.exe"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/p7zip"
RDEPEND="virtual/wine"

S="${WORKDIR}"
RESTRICT="mirror strip"

src_unpack() {
	:
}

src_compile() {
	:
}

src_install() {
	local dir=/opt/${PN%-bin}

	dodir ${dir}
	( cd "${D}"/${dir} && 7z e "${DISTDIR}/${A}" ) || die

	make_desktop_entry "wine ${dir}/XieXieFreeware.exe" 'XieXie' '' 'Game'
	mv "${D}"/usr/share/applications/{*,${PN}}.desktop || die
}
