# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit toolchain-funcs versionator eutils

MY_PN=invaders
MY_PV="$(get_version_component_range 1-3)"
PATCH_V="${PV/*_p/}"

DESCRIPTION="Multi boot compliant kernel game"
HOMEPAGE="http://www.erikyyy.de/invaders/"
SRC_URI="http://www.erikyyy.de/${MY_PN}/${MY_PN}-${MY_PV}.tar.gz
	mirror://debian/pool/main/${MY_PN:0:1}/${MY_PN}/${MY_PN}_${MY_PV}-${PATCH_V}.debian.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}"/${MY_PN}

src_prepare() {
	cp ${FILESDIR}/${MY_PN}-${MY_PV}-makefile "${S}"/Makefile || die
	epatch ../debian/patches/*.patch
	rm ${MY_PN} || die
}

src_compile() {
	emake CC=$(tc-getCC)
}

src_install() {
	dodoc README

	insinto /usr/share/doc/${PF}/examples/
	doins "${WORKDIR}"/debian/examples/grub-menu.lst

	exeinto /boot/
	doexe ${MY_PN}
}
