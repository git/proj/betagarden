# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit autotools eutils git-r3

DESCRIPTION="Parallel bzip2 utility"
HOMEPAGE="https://github.com/kjn/lbzip2/"
SRC_URI=""
EGIT_REPO_URI="git://github.com/kjn/lbzip2.git"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="symlink"

RDEPEND=""
DEPEND="
	dev-lang/perl
	dev-libs/gnulib"

src_prepare() {
	pushd src > /dev/null
	epatch \
		"${FILESDIR}"/0.23-s_isreg.patch
	popd > /dev/null

	sed \
		-e '/^aclocal/d' \
		-e '/autoconf/d' \
		-e '/uatoheader/d' \
		-e '/automake/d' \
		-i build-aux/autogen.sh || die

	bash build-aux/autogen.sh

	eautoreconf
}

src_install() {
	default

	use symlink && dosym ${PN} /usr/bin/bzip2
}
