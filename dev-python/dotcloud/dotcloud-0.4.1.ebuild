# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

MY_P="${PN}.cli-${PV}"

DESCRIPTION="dotCloud command-line interface client"
HOMEPAGE="http://www.dotcloud.com/"
SRC_URI="mirror://pypi/d/dotcloud.cli/${MY_P}.tar.gz"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}/${MY_P}"
