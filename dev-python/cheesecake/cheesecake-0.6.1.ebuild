# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="Computes goodness index for Python packages based on various empirical kwalitee factors"
HOMEPAGE="http://pycheesecake.org/"
SRC_URI="http://cheeseshop.python.org/packages/source/C/Cheesecake/${P}.tar.gz"

LICENSE="PSF-2"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE="test doc"

RDEPEND="
	>=dev-python/pylint-0.6.4[${PYTHON_USEDEP}]
	>=dev-python/setuptools-0.6_rc3[${PYTHON_USEDEP}]
	"
DEPEND="${RDEPEND}
	test? ( dev-python/nose[${PYTHON_USEDEP}] )"

python_install_all() {
	distutils-r1_python_install_all
	use doc && docinto html && dodoc -r docs/*
}

python_test() {
	esetup.py nosetests
}
