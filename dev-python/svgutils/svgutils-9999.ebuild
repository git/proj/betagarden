# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python{2_7,3_{4,5}} )

inherit git-r3 distutils-r1

MY_PN='svg_utils'  # != PN due to https://pypi.python.org/pypi/svgutils
DESCRIPTION="Python tools to create and manipulate SVG files"
HOMEPAGE="https://github.com/btel/${MY_PN}"
EGIT_REPO_URI="https://github.com/btel/${MY_PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="dev-python/lxml[${PYTHON_USEDEP}]"
