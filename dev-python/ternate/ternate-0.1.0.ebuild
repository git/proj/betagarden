# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="Tool for creating FOAF and home pages for Gentoo developers"
HOMEPAGE="http://trac.assembla.com/ternate"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="doc examples"

RDEPEND="
	dev-python/lxml[${PYTHON_USEDEP}]
	dev-python/rdflib[${PYTHON_USEDEP}]
	"
DEPEND="${RDEPEND}
	dev-python/setuptools[${PYTHON_USEDEP}]
"

python_install_all() {
	use doc && HTML_DOCS=( docs/api/* )
	use examples && EXAMPLES=( examples )
	distutils-r1_python_install_all
}

python_test() {
	esetup.py test
}
