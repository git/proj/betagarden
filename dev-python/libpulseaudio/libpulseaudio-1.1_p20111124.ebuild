# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

PYTHON_COMPAT=( python2_7 )
inherit distutils-r1

MY_PN=python-pulseaudio
MY_PV=7af33cf60f87f74851dd47359859d8c47c3f7d2d
DESCRIPTION="PulseAudio bindings for Python using ctypes"
HOMEPAGE="https://pypi.python.org/pypi/libpulseaudio"
SRC_URI="https://github.com/Valodim/${MY_PN}/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="media-sound/pulseaudio"

S="${WORKDIR}"/${MY_PN}-${MY_PV}
