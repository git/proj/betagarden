# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="Python package to interact with the MediaWiki API"
HOMEPAGE="https://code.google.com/p/python-wikitools/"
SRC_URI="https://python-${PN}.googlecode.com/files/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="
	dev-python/simplejson[${PYTHON_USEDEP}]
	dev-python/poster[${PYTHON_USEDEP}]"
