# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python{2_7,3_4,3_5} )

inherit distutils-r1

DESCRIPTION="A nosetests plugin with a progress bar and emphasis important"
HOMEPAGE="https://github.com/erikrose/nose-progressive"
SRC_URI="https://github.com/erikrose/nose-progressive/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	<dev-python/blessings-2.0[${PYTHON_USEDEP}]
	>=dev-python/nose-1.2.1[${PYTHON_USEDEP}]
	"
RDEPEND="${DEPEND}"
