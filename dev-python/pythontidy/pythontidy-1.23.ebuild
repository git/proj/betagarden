# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit python-r1

MY_PN=PythonTidy

DESCRIPTION="Cleans up, regularizes, and reformats the text of Python scripts"
HOMEPAGE="http://pypi.python.org/pypi/PythonTidy/"
SRC_URI="http://lacusveris.com/${MY_PN}/${MY_PN}-${PV}.python"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S="${WORKDIR}"

src_install() {
	python_foreach_impl python_newscript "${DISTDIR}"/${A} ${PN}
	python_foreach_impl python_optimize
}
