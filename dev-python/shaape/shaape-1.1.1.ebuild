# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="ASCII art to image converter; to be used with asciidoc"
HOMEPAGE="https://github.com/christiangoltz/shaape"
SRC_URI="https://github.com/christiangoltz/shaape/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-python/networkx[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/pycairo[${PYTHON_USEDEP}]
	dev-python/pygtk[${PYTHON_USEDEP}]
	dev-python/pyyaml[${PYTHON_USEDEP}]
	dev-python/setuptools[${PYTHON_USEDEP}]
	sci-libs/scipy[${PYTHON_USEDEP}]
	"
RDEPEND="${DEPEND}"

src_install() {
	distutils-r1_src_install

	insinto /usr/share/asciidoc/filters/${PN}/
	doins asciidoc-filter/shaape-filter.conf

	insinto /usr/share/${PN}/examples
	doins -r shaape/tests/input/*
}
