# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit autotools-utils git-r3 python-single-r1

DESCRIPTION="Python binding to the GUDev udev helper library"
HOMEPAGE="http://github.com/nzjrs/python-gudev"
SRC_URI=""
EGIT_REPO_URI="git://github.com/nzjrs/python-gudev.git"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${PYTHON_DEPS}
	>=sys-fs/udev-147
	dev-python/pygobject:2[${PYTHON_USEDEP}]"
DEPEND="${RDEPEND}
	virtual/pkgconfig"
