# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

DESCRIPTION="Library for working with graphs in Python"
HOMEPAGE="http://code.google.com/p/python-graph/"
SRC_URI="http://python-graph.googlecode.com/files/${P}.zip"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE="" # TODO tests

S="${WORKDIR}"/${PN}

src_prepare() {
	local dir
	for dir in core dot; do
		pushd ${dir} > /dev/null
			distutils-r1_src_prepare
		popd > /dev/null
	done
}

src_configure() {
	local dir
	for dir in core dot; do
		pushd ${dir} > /dev/null
			distutils-r1_src_configure
		popd > /dev/null
	done
}

src_compile() {
	local dir
	for dir in core dot; do
		pushd ${dir} > /dev/null
			distutils-r1_src_compile
		popd > /dev/null
	done
}

src_install() {
	local dir
	for dir in core dot; do
		pushd ${dir} > /dev/null
			distutils-r1_src_install
		popd > /dev/null
	done
}
