# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit python-single-r1

DESCRIPTION="A system for interactive experimentation with Python"
HOMEPAGE="http://fishsoup.net/software/reinteract/"
SRC_URI="http://download.reinteract.org/sources/${P}.tar.bz2"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="sound"

DEPEND=""
RDEPEND="
	dev-python/pygtk[${PYTHON_USEDEP}]
	dev-python/numpy[${PYTHON_USEDEP}]
	dev-python/matplotlib[${PYTHON_USEDEP}]
	sound? ( media-sound/sox )"

src_install() {
	default
	python_optimize
}
