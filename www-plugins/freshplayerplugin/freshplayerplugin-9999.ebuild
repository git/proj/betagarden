# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit git-r3 cmake-utils

DESCRIPTION="Chromium Flash wrapper for Mozilla Firefox"
HOMEPAGE="https://github.com/i-rinat/freshplayerplugin"
SRC_URI=""
EGIT_REPO_URI="https://github.com/i-rinat/${PN}.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

COMMON_DEPEND="
	dev-libs/glib:2
	dev-libs/libconfig
	dev-libs/libevent[threads]
	dev-libs/uriparser
	media-libs/alsa-lib
	media-libs/freetype
	media-libs/mesa[egl,gles2]
	x11-libs/cairo
	x11-libs/gtk+:2
	x11-libs/libX11
	x11-libs/libXinerama
	x11-libs/pango
	"
DEPEND="${COMMON_DEPEND}
	dev-util/ragel
	"
RDEPEND="${COMMON_DEPEND}
	|| (
		www-plugins/chrome-binary-plugins[flash]
		www-client/google-chrome
		www-client/google-chrome-beta
		www-client/google-chrome-unstable
	)
	"

src_install() {
	insinto /etc
	newins data/freshwrapper.conf.example freshwrapper.conf
	ewarn 'Depending on what branch of Google Chrome you use, the path to libpepflashplayer.so may need editing in /etc/freshwrapper.conf .'

	insinto /usr/lib/nsbrowser/plugins/
	doins "${BUILD_DIR}"/libfreshwrapper-pepperflash.so
}
