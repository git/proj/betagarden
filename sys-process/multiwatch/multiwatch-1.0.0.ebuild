# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit autotools git-r3

DESCRIPTION="multiwatch forks and watches multiple instances of a program"
HOMEPAGE="http://redmine.lighttpd.net/projects/multiwatch"
EGIT_REPO_URI="git://git.lighttpd.net/multiwatch.git"
EGIT_COMMIT="80d85493"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-libs/libev
	dev-libs/glib:2"
RDEPEND="${DEPEND}"

src_prepare() {
	eautoreconf
}

src_install() {
	default
	doman multiwatch.1
}
