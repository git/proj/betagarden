# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit eutils

DESCRIPTION="Dyndns client in C supporting various services"
HOMEPAGE="http://www.inatech.eu/inadyn/"
SRC_URI="http://www.inatech.eu/${PN}/${PN}.v${PV}.zip"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND=""

S="${WORKDIR}"/${PN}

pkg_setup() {
	enewuser inadyn -1 -1 -1
}

src_prepare() {
	rm -R bin || die
	cp "${FILESDIR}"/${P}-makefile src/Makefile || die
}

src_compile() {
	cd src || die
	emake || die
}

src_install() {
	dosbin src/inadyn || die
	doman man/* || die
	dohtml readme.html || die

	newinitd "${FILESDIR}"/inadyn.initd inadyn || die

	insinto /etc
	doins "${FILESDIR}"/inadyn.conf || die
}

pkg_postinst() {
	elog "You will need to edit /etc/inadyn.conf file before"
	elog "running inadyn for the first time. The file is basically"
	elog "command line options; see inadyn and inayn.conf manpages."
}
