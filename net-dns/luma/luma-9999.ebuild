# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1 git-r3

DESCRIPTION="LDAP GUI base on PyQt4"
HOMEPAGE="http://luma.sourceforge.net/"
SRC_URI=""
EGIT_REPO_URI="git://${PN}.git.sourceforge.net/gitroot/${PN}/${PN}"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND=""
RDEPEND="
	dev-python/python-ldap[${PYTHON_USEDEP}]
	dev-python/PyQt4[${PYTHON_USEDEP}]"
