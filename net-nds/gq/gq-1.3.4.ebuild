# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"

DESCRIPTION="A GTK+-based LDAP client"
HOMEPAGE="http://www.gq-project.org/"
SRC_URI="mirror://sourceforge/gqclient/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="kerberos"

RDEPEND=">=x11-libs/gtk+-2.6
	net-nds/openldap
	kerberos? ( virtual/krb5 )
	dev-libs/openssl
	dev-libs/libxml2
	>=dev-libs/glib-2.6
	x11-libs/pango
	dev-libs/cyrus-sasl
	gnome-base/gnome-keyring
	gnome-base/libglade
	dev-libs/libgcrypt"
DEPEND="${RDEPEND}
	app-text/gnome-doc-utils
	dev-util/intltool
	virtual/pkgconfig"

src_configure() {
	local myconf="--enable-browser-dnd --enable-cache --disable-update-mimedb"

	# --without-kerberos-prefix is broken, working around it
	use kerberos && myconf="${myconf} --with-kerberos-prefix=/usr"

	econf ${myconf} || die
}

src_install() {
	rm -f "${D}"/usr/share/locale/locale.alias
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS ChangeLog NEWS README* TODO
}
