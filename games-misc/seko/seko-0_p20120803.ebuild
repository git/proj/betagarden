# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils toolchain-funcs games

MY_PV=68ae6f27b8a7dbb52399308a1fb4e228b970d975
DESCRIPTION="Dance game"
HOMEPAGE="http://pouet.net/prod.php?which=59597"
SRC_URI="https://github.com/japeq/${PN}/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	media-libs/libsdl
	media-libs/freetype
	virtual/opengl
	virtual/glu
	media-libs/libvorbis
	media-libs/libpng:0
	media-libs/glew
	net-misc/curl
	dev-libs/openssl:0"
DEPEND="virtual/pkgconfig
	${RDEPEND}"

S=${WORKDIR}/${PN}-${MY_PV}

src_prepare() {
	epatch \
		"${FILESDIR}"/${P}-data-path.patch \
		"${FILESDIR}"/${P}-flags.patch
}

src_compile() {
	emake CXX="$(tc-getCXX)"
}

src_install() {
	insinto "${GAMES_DATADIR}"/${PN}/
	doins -r data
	newgamesbin ${PN}-linux ${PN}
	dodoc README
}
