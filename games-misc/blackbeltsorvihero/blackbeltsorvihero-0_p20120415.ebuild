# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit eutils toolchain-funcs games python-single-r1

MY_PN=BlackBeltSorviHero
MY_PV=00c9b594d68c1e073b69b492be3e814d08ae14e1
DESCRIPTION="Wood turning game"
HOMEPAGE="http://pouet.net/prod.php?which=57427"
SRC_URI="https://github.com/japeq/${PN}/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${PYTHON_DEPS}
	virtual/opengl
	virtual/glu
	media-libs/glew
	media-libs/sdl-ttf
	media-libs/libvorbis
	media-libs/libpng:0"
DEPEND="${RDEPEND}"

S=${WORKDIR}/${PN}-${MY_PV}

src_prepare() {
	epatch \
		"${FILESDIR}"/${P}-data-path.patch \
		"${FILESDIR}"/${P}-flags.patch
}

src_compile() {
	emake CXX="$(tc-getCXX)" VERSION="${PV}"
}

src_install() {
	insinto "${GAMES_DATADIR}"/${PN}/
	doins ${MY_PN}.dat
	newgamesbin ${MY_PN}.bin ${PN}
	dodoc README

	doicon ${MY_PN}.png
	domenu ${MY_PN}.desktop
}
