# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

inherit flag-o-matic games

DESCRIPTION="Karaoke game for your PC"
HOMEPAGE="http://ultrastardx.sourceforge.net/"
SRC_URI="http://nerdserv.de/${P}.tar.xz"

LICENSE="GPL-2+ || ( GPL-3 Apache-2.0 ) BitstreamVera CC-BY-SA-3.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="projectm"

RDEPEND="
	media-libs/libpng:0
	media-libs/libsdl[opengl]
	media-libs/freetype
	dev-db/sqlite:3
	dev-lang/lua
	<=media-video/ffmpeg-1
	projectm? ( media-libs/libprojectm )
	media-libs/portaudio
	dev-libs/libpcre
	virtual/opengl"
DEPEND="${RDEPEND}
	dev-lang/fpc
	x11-misc/xdg-utils
	virtual/pkgconfig"

pkg_setup() {
	filter-ldflags '-Wl,-O*' '-Wl,--hash-style=*' '-Wl,--as-needed'
	games_pkg_setup
}

src_configure() {
	egamesconf $(use_with projectm libprojectM)
}

src_install() {
	default

	dodoc AUTHORS.txt ChangeLog.GERMAN.txt ChangeLog.txt README.txt || die
	rm "${D}/${GAMES_DATADIR}"/${PN}/COPYING.txt || die

	domenu dists/${PN}.desktop || die
	newicon -s scalable icons/${PN}-icon.svg ${PN}.svg || die
	newicon -s 32 icons/${PN}-icon_32.png ${PN}.png || die
	newicon -s 256 icons/${PN}-icon_256.png ${PN}.png || die

	# Workaround for XFCE panel
	dosym /usr/share/icons/hicolor/scalable/apps/${PN}.svg /usr/share/pixmaps/${PN} || die

	xdg-icon-resource forceupdate

	prepgamesdirs
}
