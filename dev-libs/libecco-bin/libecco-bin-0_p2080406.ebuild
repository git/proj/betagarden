# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="ECCO (Encyclopedia of Chinese Chess Opening) Analysis"
HOMEPAGE="http://www.xqbase.com/ecco/ecco_dll.htm"
SRC_URI="http://www.xqbase.com/ecco/ecco_dll.7z"

LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/p7zip"
RDEPEND=""

S="${WORKDIR}"
RESTRICT="mirror"

QA_PREBUILT="/usr/lib*/libecco.so"

src_install() {
	dolib libecco.so || die
}
