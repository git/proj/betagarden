# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit cmake-utils

DESCRIPTION="Command line parser for Qt (like getopt)"
HOMEPAGE="http://xf.iksaif.net/dev/qcommandline.html"
SRC_URI="http://xf.iksaif.net/dev/${PN}/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=dev-qt/qtcore-4.6:4"
RDEPEND="${DEPEND}"
