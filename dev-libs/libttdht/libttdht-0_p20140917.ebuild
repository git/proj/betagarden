# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils autotools

DESCRIPTION="BitTorrent library"
HOMEPAGE="https://github.com/naihe2010/libttdht"
SRC_URI="http://www.hartwork.org/public/${P}.tar.xz"

LICENSE="GPL-2+ BSD"  # BSD for queue.h
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="dev-libs/openssl:0"
DEPEND="${RDEPEND}
	app-arch/xz-utils"

src_prepare() {
	epatch "${FILESDIR}"/${P}-makefile.patch
	eautoreconf
	rm -R win32 || die
}

src_install() {
	default
	mv "${ED}"/include "${ED}"/usr/include || die
}
