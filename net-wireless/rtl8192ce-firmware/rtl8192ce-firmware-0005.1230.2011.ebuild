# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

DESCRIPTION="RTL8192CE wireless chipset firmware"
HOMEPAGE="http://www.realtek.com.tw/search/default.aspx?keyword=8192ce"
SRC_URI="ftp://WebUser:wK9xBuD5@209.222.7.36/cn/wlan/92ce_se_de_linux_mac80211_${PV}.tar.gz"

LICENSE="freedist"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

S="${WORKDIR}"/rtl_92ce_92se_92de_linux_mac80211_${PV}

src_prepare() {
	rm Makefile || die
}

src_install() {
	insinto /lib/firmware/rtlwifi
	doins firmware/rtlwifi/{rtl8192cfw.bin,rtl8192cfwU.bin,rtl8192cfwU_B.bin,Realtek-Firmware-License.txt} || die
}
