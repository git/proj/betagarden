# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

MY_P=ocs-${PV}

DESCRIPTION="OPAL functional programming language"
HOMEPAGE="http://uebb.cs.tu-berlin.de/~opal/"
SRC_URI="http://projects.uebb.tu-berlin.de/opal/trac/raw-attachment/wiki/OCS/${MY_P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~sparc ~x86"
IUSE="X doc +tk +tcl"

REQUIRED_USE="tk? ( tcl )"

DEPEND="
	>=sys-libs/ncurses-5.4-r5:0=
	>=sys-libs/readline-4.3-r5:0=
	tcl? ( =dev-lang/tcl-8.5*:* )
	tk? ( =dev-lang/tk-8.5*:* )
	"

S="${WORKDIR}/${MY_P}"

src_configure() {
	econf \
		$(use_enable doc) \
		$(use_enable doc dosfop) \
		--enable-opalwin \
		--enable-oasys \
		--enable-dynamic \
		--disable-absolute-pathes \
		--prefix="${D}/usr"
}

src_compile() {
	:
}

src_install() {
	# chmod:     /var <-- Prevent access violation
	addpredict /var
	mkdir -p "${D}/usr/" || die

	emake -j1 install

	# Fix man folder location
	if [[ -d "${D}"/usr/man/ ]]; then
		dodir /usr/share/
		mv "${D}"/usr/{,share/}man/ || die
	fi

	# Fix "OCS=${D}//usr" in binaries (due to broken --disable-absolute-pathes)
	sed \
		-e 's|^OCS=.*|OCS=/usr|' \
		-i "${D}"/usr/bin/{dosfop,oasys,oasys1,ocs,pureDosfop} || die
}
