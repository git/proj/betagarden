# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1 versionator

MY_PV="$(get_version_component_range 1-2)"

DESCRIPTION="Linux Backup, Done Right"
HOMEPAGE="https://launchpad.net/time-drive"
SRC_URI="http://launchpad.net/${PN}/${MY_PV}/${MY_PV}/+download/Time-Drive-${PV}.zip"

SLOT="0"
KEYWORDS="~amd64 ~x86 ~amd64-linux ~x86-linux"
LICENSE="GPL-2"
IUSE=""

RDEPEND="
	app-backup/duplicity[${PYTHON_USEDEP}]
	dev-python/PyQt4[${PYTHON_USEDEP}]"
DEPEND="app-arch/unzip"
