# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python2_7 )

inherit git-r3 linux-info distutils-r1

DESCRIPTION="A personal Dropbox replacement based on Git"
HOMEPAGE="http://www.mayrhofer.eu.org/dvcs-autosync"
SRC_URI=""
EGIT_REPO_URI="
	git://gitorious.org/dvcs-autosync/dvcs-autosync.git
	https://git.gitorious.org/dvcs-autosync/dvcs-autosync.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	dev-python/pyinotify[${PYTHON_USEDEP}]
	dev-python/xmpppy[${PYTHON_USEDEP}]"
RDEPEND="${DEPEND}"

CONFIG_CHECK="INOTIFY_USER"
