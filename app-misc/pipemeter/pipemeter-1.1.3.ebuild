# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="File pipe progress meter"
HOMEPAGE="http://spamaps.org/pipemeter.php"
SRC_URI="http://spamaps.org/files/${PN}/${P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_install() {
	doman ${PN}.1
	dodoc Changelog README
	dobin ${PN}
}
