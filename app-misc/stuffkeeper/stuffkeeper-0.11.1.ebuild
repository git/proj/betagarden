# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils

DESCRIPTION="Generic catalog program"
HOMEPAGE="http://stuffkeeper.org/"
SRC_URI="http://download.sarine.nl/Programs/StuffKeeper/${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE="spell debug"

RDEPEND="
	x11-libs/gtk+:2
	dev-libs/glib:2
	dev-db/sqlite:3
	gnome-base/libglade:2.0
	dev-util/gob:2
	spell? ( app-text/gtkspell )"
DEPEND="${RDEPEND}
	dev-util/intltool
"

src_configure() {
	econf $(use_enable debug)
}
