# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit multilib

DESCRIPTION="Configurably disable the ability of a process to access the internet"
HOMEPAGE="http://sourceforge.net/projects/netjail/"
SRC_URI="mirror://sourceforge/project/${PN}/${PN}/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_install() {
	local preload_dir="/usr/$(get_libdir)/${PN}"
	local preload_file="lib${PN}.so.1.0.0"

	dodoc AUTHORS README || die

	insinto ${preload_dir}
	doins ${preload_file} || die

	cat <<-EOF > ${PN}
		#! /bin/bash
		[[ -z \${NETJAIL_INET} ]] && export NETJAIL_INET=D:ALL
		[[ -z \${NETJAIL_UNIX} ]] && export NETJAIL_UNIX=D:ALL
		LD_PRELOAD="${preload_dir}/${preload_file}" "\$@"
	EOF
	chmod a+x ${PN} || die
	dobin ${PN} || die
}
