# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit toolchain-funcs

MY_PV=830943317e1c2ab28111ca6ced79e2bbd3624fc6

DESCRIPTION="Multimedia Presenter for Lua"
HOMEPAGE="http://info-beamer.org/"
SRC_URI="https://github.com/dividuum/${PN}/archive/${MY_PV}.tar.gz"

LICENSE="all-rights-reserved"  # LICENSE.txt needs closer inspection
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=dev-lang/lua-5.1:0 <dev-lang/lua-5.2:0
	media-libs/freetype
	dev-libs/libevent
	<media-libs/glfw-3
	media-libs/mesa
	media-libs/glu
	media-libs/glew
	media-libs/ftgl
	media-libs/devil
	media-video/ffmpeg:0
	sys-libs/zlib"

DEPEND="${RDEPEND}
	app-text/ronn"

S="${WORKDIR}/${PN}-${MY_PV}"

src_compile() {
	emake \
			CC="$(tc-getCC)" \
			VERSION="${PV}" \
			LUA_CFLAGS="" \
			LUA_LDFLAGS="-llua"
	emake \
			VERSION="${PV}" \
			${PN}.1
}

src_install() {
	dodoc README.md LICENSE.txt ChangeLog
	doman ${PN}.1

	insinto /usr/share/doc/${PF}/
	doins -r doc samples

	dodir /usr/bin
	emake \
			DESTDIR="${D}" \
			prefix=/usr \
			VERSION="${PV}" \
			install
}
