# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils toolchain-funcs

DESCRIPTION="Multimedia Presenter for Lua"
HOMEPAGE="http://info-beamer.org/"
SRC_URI="http://www.hartwork.org/public/${P}.tar.xz"

LICENSE="all-rights-reserved"  # LICENSE.txt needs closer inspection
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=dev-lang/lua-5.1:0 <dev-lang/lua-5.2:0
	media-libs/freetype
	dev-libs/libevent
	media-libs/glfw
	media-libs/mesa
	media-libs/glu
	media-libs/glew
	media-libs/ftgl
	media-libs/devil
	media-video/ffmpeg:0
	sys-libs/zlib"

RDEPEND="${DEPEND}"

src_prepare() {
	epatch "${FILESDIR}"/${P}-makefile.patch
	rm doc/.gitignore
}

src_compile() {
	emake CC="$(tc-getCC)" VERSION="${PV}"
}

src_install() {
	dodoc README.md LICENSE.txt ChangeLog

	insinto /usr/share/doc/${PF}/
	doins -r doc samples

	dodir /usr/bin
	emake DESTDIR="${D}" prefix=/usr install
}
