# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="Command-line based markdown presentation tool."
HOMEPAGE="https://github.com/visit1985/mdp"
SRC_URI="https://github.com/visit1985/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_compile() {
	emake CFLAGS="${CFLAGS} -I../include" LDFLAGS="${LDFLAGS}"
}

src_install() {
	emake PREFIX="${D}" install || die  # Makefile confuses up ${DESTDIR} and ${PREFIX}

	insinto /usr/share/${PN}/
	doins sample.md || die
}
