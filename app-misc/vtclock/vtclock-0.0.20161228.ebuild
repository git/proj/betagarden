# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A giant text-mode clock"
HOMEPAGE="https://github.com/dse/vtclock"
SRC_URI="https://github.com/dse/vtclock/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-libs/ncurses:*"
RDEPEND="${DEPEND}"

DOCS=( README.md )

src_install() {
	emake DESTDIR="${D}" prefix=/usr install
}
