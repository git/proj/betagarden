# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

PYTHON_COMPAT=( python2_7 )
inherit distutils-r1

DESCRIPTION="Tool to generate Xiangqi (Chinese chess) setup graphics"
HOMEPAGE="https://github.com/hartwork/xiangqi-setup"
SRC_URI="https://github.com/hartwork/xiangqi-setup/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="AGPL-3+
	CC0-1.0 public-domain
	|| ( FDL-1.2+ CC-BY-SA-4.0 )
	non-free? ( xiangqi-font )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="non-free"

RDEPEND="dev-python/svgutils"

src_install() {
	distutils-r1_src_install
	if ! use non-free ; then
		rm -R "${D}"/usr/share/${PN}/themes/pieces/latex_xqlarge_*/ || die
	fi
}
