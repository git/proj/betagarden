# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="eXtra fast Directory changer"
HOMEPAGE="https://fbb-git.github.io/xd/"
# Note: Upstreamm is a Debian dev, latest tarball not released upstream
SRC_URI="https://github.com/fbb-git/xd/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND="
	dev-util/icmake
	dev-libs/libbobcat
	doc? ( app-doc/yodl )"
RDEPEND="${DEPEND}"

S="${WORKDIR}"/${P}/${PN}

src_prepare() {
	sed 's|^#include "icmake/library"$|#define COPT "--std=c++0x -Wall '"${CXXFLAGS}"'"\n\0|' -i build \
		|| die 'sed failed (file missing)'
}

src_compile() {
	./build program || die './build program'
	./build library || die './build library'

	if use doc; then
		./build man || die './build man'
	fi
}

src_install() {
	./build install "${D}"
}
