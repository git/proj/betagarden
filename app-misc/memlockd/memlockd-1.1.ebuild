# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="Daemon that locks files into memory"
HOMEPAGE="http://doc.coker.com.au/projects/memlockd/"
SRC_URI="http://www.coker.com.au/${PN}/${PN}_${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

src_install() {
	doman memlockd.8
	newdoc changes.txt ChangeLog

	# TODO: adjust default config to Gentoo
	insinto /etc/
	doins memlockd.cfg

	dosbin memlockd

	doinitd "${FILESDIR}"/${PN}
}
