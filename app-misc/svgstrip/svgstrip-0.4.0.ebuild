# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit python-r1

DESCRIPTION="Strips private information from SVG files"
HOMEPAGE="https://github.com/hartwork/svgstrip"
SRC_URI="https://github.com/hartwork/svgstrip/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${PYTHON_DEPS}"
DEPEND="${DEPEND}"

src_install() {
	python_foreach_impl python_doscript ${PN}
}
