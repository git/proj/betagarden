# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

USE_RUBY=( ruby19 )
inherit ruby-ng

DESCRIPTION="Text presentation program (ncurses-based)"
HOMEPAGE="http://www.ngolde.de/tpp.html"
SRC_URI="http://www.ngolde.de/download/${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="dev-ruby/ncurses-ruby"

S="${WORKDIR}"/all/${P}

src_prepare() {
	local ruby=$(ruby_implementation_command ruby19)
	sed "1c\#! ${ruby}" -i tpp.rb || die
}

src_compile() {
	:
}

src_install() {
	dodir /usr/bin || die
	dodir /usr/share/man/man1 || die
	docompress -x /usr/share/doc/${PN}/examples/
	docompress -x /usr/share/doc/${PN}/contrib/

	emake prefix="${D}"/usr install
}
