# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils

DESCRIPTION="smooth Class Library"
HOMEPAGE="http://www.smooth-project.org/"
SRC_URI="mirror://sourceforge/project/${PN}/${PN}/${PV}/${P}.zip"

LICENSE="Artistic-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-libs/atk
	dev-libs/fribidi
	dev-libs/glib:*
	dev-libs/libxml2
	media-libs/fontconfig
	media-libs/freetype
	media-libs/libpng:*
	sys-devel/gcc:*[cxx]
	sys-libs/zlib
	virtual/jpeg:*
	x11-libs/cairo
	x11-libs/gdk-pixbuf
	x11-libs/gtk+:2
	x11-libs/libX11
	x11-libs/libXmu
	x11-libs/libXt
	x11-libs/pango
	"
DEPEND="${RDEPEND}"

DOCS=( ChangeLog ToDo.xml )

src_prepare() {
	epatch "${FILESDIR}"/${P}-compile-flags.patch
	epatch "${FILESDIR}"/${P}-destdir.patch
	epatch "${FILESDIR}"/${P}-ldconfig.patch
	epatch "${FILESDIR}"/${P}-no-uninstall.patch
}

src_compile() {
	emake
	emake -C tools/translator
}

src_install() {
	emake DESTDIR="${D}" PREFIX=/usr install

	insinto /usr/share/${PF}/
	doins -r doc samples tutorial
}
