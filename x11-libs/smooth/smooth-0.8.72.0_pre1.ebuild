# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils

MY_PV=${PV/_/-}
DESCRIPTION="smooth Class Library"
HOMEPAGE="http://www.smooth-project.org/"
SRC_URI="https://freac.org/preview/${PN}-${MY_PV}.tar.gz"

LICENSE="Artistic-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-libs/atk
	dev-libs/fribidi
	dev-libs/glib:*
	dev-libs/libxml2
	media-libs/fontconfig
	media-libs/freetype
	media-libs/libpng:*
	sys-devel/gcc:*[cxx]
	sys-libs/zlib
	virtual/jpeg:*
	x11-libs/cairo
	x11-libs/gdk-pixbuf
	x11-libs/gtk+:2
	x11-libs/libX11
	x11-libs/libXmu
	x11-libs/libXt
	x11-libs/pango
	"
DEPEND="${RDEPEND}"

DOCS=( ChangeLog ToDo.xml )

S="${WORKDIR}"/${PN}-${MY_PV}

src_compile() {
	emake prefix=/usr
}

src_install() {
	addpredict /etc/ld.so.cache:/etc/ld.so.cache~

	emake DESTDIR="${D}" prefix=/usr install

	insinto /usr/share/${PF}/
	doins -r doc samples tutorial
}
