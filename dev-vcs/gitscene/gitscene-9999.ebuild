# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit git-r3 python-r1

DESCRIPTION="A GTK based repository browser for git"
HOMEPAGE="http://github.com/pta/gitscene"
SRC_URI=""
EGIT_REPO_URI="git://github.com/pta/gitscene.git"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-python/pygtk[${PYTHON_USEDEP}]
	dev-python/pycairo[${PYTHON_USEDEP}]
	dev-python/pygobject:3[${PYTHON_USEDEP}]
	dev-python/pygtksourceview[${PYTHON_USEDEP}]"
RDEPEND="${DEPEND}"

src_install() {
	python_foreach_impl python_doscript ${PN}
	dodoc ${PN}.txt
}
