# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

USE_RUBY="ruby18"

inherit ruby-fakegem

DESCRIPTION="Simple tool to help track git and svn vendor branches in a git repository"
HOMEPAGE="http://github.com/evilchelu/braid"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""
IUSE=""

ruby_add_rdepend "
	>=dev-ruby/main-2.8.0
	>=dev-ruby/open4-0.9.6"
