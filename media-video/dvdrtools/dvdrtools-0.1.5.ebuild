# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

DESCRIPTION="DVD writing program + minimal authoring"
HOMEPAGE="https://savannah.nongnu.org/projects/dvdrtools/"
SRC_URI="http://web.cvs.savannah.gnu.org/viewvc/*checkout*/${PN}/files/${PN}_${PV}.tar.gz?root=${PN} -> ${PN}_${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="!app-cdr/cdrtools"

S="${WORKDIR}"/${PN}