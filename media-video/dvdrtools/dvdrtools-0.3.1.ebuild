# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

DESCRIPTION="DVD writing program + minimal authoring"
HOMEPAGE="https://savannah.nongnu.org/projects/dvdrtools/"
SRC_URI="mirror://debian/pool/non-free/${PN:0:1}/${PN}/${PN}_${PV}.orig.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="minimal"

DEPEND=""
RDEPEND="!minimal? ( !app-cdr/cdrtools )"

src_install() {
	default

	use minimal && {
		rm "${D}"/usr/bin/{cdda2{wav,ogg,mp3},devdump,iso{dump,info,vfy},mkisofs,readcd} || die
		rm "${D}"/usr/share/man/man1/{cdda2wav,readcd}.1 || die
		rm "${D}"/usr/share/man/man8/{isoinfo,mkisofs}.8 || die
	}
}
