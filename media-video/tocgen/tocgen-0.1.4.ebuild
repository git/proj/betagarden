# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

inherit autotools

MY_PN=dvdrtools
DESCRIPTION="Tool to generate files VIDEO_TS/VIDEO_TS.{BUP,IFO} where missing"
HOMEPAGE="https://savannah.nongnu.org/projects/dvdrtools/"
SRC_URI="http://web.cvs.savannah.gnu.org/viewvc/*checkout*/${MY_PN}/files/${MY_PN}_${PV}.tar.gz?root=${MY_PN} -> ${MY_PN}_${PV}.tar.gz
	http://www.hartwork.org/public/${MY_PN}_${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="!<media-video/dvdrtools-0.1.5"

S="${WORKDIR}"/${MY_PN}

src_prepare() {
	eautoreconf
}

src_compile() {
	emake -C video tocgen || die
}

src_install() {
	dobin video/tocgen || die
}
