# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit autotools

MY_PV=f8d8844d52387b3db7b8736f5e86156d9374f781
DESCRIPTION="utility for grabbing JPEGs from V4L2 devices"
HOMEPAGE="https://github.com/twam/v4l2grab"
SRC_URI="https://github.com/twam/${PN}/archive/${MY_PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	media-libs/libv4l
	virtual/jpeg:*"
RDEPEND="${DEPEND}"

S="${WORKDIR}"/${PN}-${MY_PV}

src_prepare() {
	eautoreconf
}
