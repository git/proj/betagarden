# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit git-r3 vim-plugin

DESCRIPTION="Ultimate hex editing system with Vim"
HOMEPAGE="https://github.com/Shougo/vinarise"
SRC_URI=""
EGIT_REPO_URI="
	git://github.com/Shougo/vinarise.git
	https://github.com/Shougo/vinarise.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="app-vim/unite"

VIM_PLUGIN_HELPFILES="vinarise"

src_prepare() {
	rm -rf .git autoload/vital.vim || die
}
