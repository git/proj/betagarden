# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit flag-o-matic

DESCRIPTION="Uses LD_PRELOAD to hide .svn directories from readdir() and readdir_r()"
HOMEPAGE="http://80386.nl/projects/hidesvn/"
SRC_URI="http://80386.nl/projects/${PN}/distfiles/${P}.tar.xz"

LICENSE="BSD-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/xz-utils"
RDEPEND=""

pkg_setup() {
	append-cflags -fPIC || die
	append-ldflags -Wl,-soname=libhidesvn.so || die
}

src_install() {
	cat <<-EOF > hidesvn
		#!/bin/sh
		LD_PRELOAD=/usr/$(get_libdir)/libhidesvn.so exec "\$@"
	EOF
	dobin hidesvn || die

	dolib libhidesvn.so || die
}
