# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit toolchain-funcs

DESCRIPTION="performs random block reads on a block device"
HOMEPAGE="http://www.linuxinsight.com/how_fast_is_your_disk.html"
SRC_URI="http://www.linuxinsight.com/files/seeker.c -> ${P}.c"

LICENSE="fairuse"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

src_compile() {
	$(tc-getCC) ${CFLAGS} ${LDFLAGS} "${DISTDIR}"/${P}.c -o ${PN} || die
}

src_install() {
	dobin ${PN} || die
}
