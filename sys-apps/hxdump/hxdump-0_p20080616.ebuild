# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"

inherit toolchain-funcs

DESCRIPTION="Hex dumper"
HOMEPAGE="http://reality.sgiweb.org/davea/software.html#hxdump"
SRC_URI="http://reality.sgiweb.org/davea/hxdump.c -> ${P}.c"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=""

src_compile() {
	"$(tc-getCC)" "${DISTDIR}"/${P}.c -include /usr/include/getopt.h \
			${CFLAGS} ${LDFLAGS} -o ${PN} || die
}

src_install() {
	dobin ${PN} || die
}
