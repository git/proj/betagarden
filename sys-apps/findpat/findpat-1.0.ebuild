# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Easy to use frontend to find"
HOMEPAGE="http://blog.hartwork.org/?p=272"
SRC_URI="http://www.hartwork.org/public/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
