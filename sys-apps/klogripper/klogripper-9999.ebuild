# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3

DESCRIPTION="Kernel log ripper, i.e. a union of dmesg and tail -f"
HOMEPAGE="https://github.com/hartwork/klogripper"
SRC_URI=""
EGIT_REPO_URI="https://github.com/hartwork/klogripper.git"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="virtual/linux-sources"

src_install() {
	dobin klogripper || die
}
