# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"
inherit eutils

DESCRIPTION="TCP port forwarder (and traffic spy)"
HOMEPAGE="http://www.vakuumverpackt.de/tcptunnel/"
SRC_URI="http://www.vakuumverpackt.de/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

#DEPEND
#RDEPEND

src_prepare() {
	epatch "${FILESDIR}"/${PN}-0.6-makefile.patch
}

src_install() {
	emake BINDIR="${D}"/usr/bin STRIP=/bin/true install || die
	dodoc README AUTHORS || die
}
