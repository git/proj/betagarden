# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit multilib

DESCRIPTION="Domain availabilty check client tool and C library"
HOMEPAGE="http://dchk.pfp.de/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-libs/libxml2"
RDEPEND="${RDEPEND}"

src_compile() {
	emake incdir=include release || die  # TODO target "gui"
}

src_install() {
	local libdir=$(get_libdir)

	dodir /usr/{bin,lib,include}
	emake prefix="${D}/usr" install || die

	[[ ${libdir} != lib ]] && {
		mv "${D}"/usr/{lib,${libdir}} || die
	}

	dodoc HISTORY.TXT || die

	insinto /usr/share/doc/${PF}
	doins -r documentation/html || die
}