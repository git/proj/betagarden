# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="Route all gateway traffic to us"
HOMEPAGE="http://nakkaya.com/makeWay.html"
SRC_URI="https://github.com/nakkaya/makeWay/tarball/${PV} -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="net-libs/libpcap"
RDEPEND="${DEPEND}"

S="${WORKDIR}/nakkaya-makeWay-0182caa"

src_compile() {
	emake -j1 linux
}

src_install() {
	dodoc README.md
	dobin build/makeWay
}
