# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit eutils java-pkg-2 java-ant-2

DESCRIPTION="Keeps a record of IP/MAC address pairings"
HOMEPAGE="http://nakkaya.com/mocha.html"
SRC_URI="https://github.com/nakkaya/${PN}/tarball/${PV} -> ${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

COMMON_DEP="java-virtuals/javamail:0
	java-virtuals/jaf:0
	dev-java/swing-layout:1"

DEPEND=">=virtual/jdk-1.5
	${COMMON_DEP}"
RDEPEND=">=virtual/jre-1.5
	sys-apps/net-tools
	${COMMON_DEP}"

S=${WORKDIR}/nakkaya-${PN}-914b1aa

java_prepare() {
	epatch \
		"${FILESDIR}"/${PN}-1.1.1-gtk.patch \
		"${FILESDIR}"/${PN}-1.1.1-gentoo.patch

	cd extLibs || die
	rm *.jar || die
	java-pkg_jar-from javamail
	java-pkg_jar-from jaf
	java-pkg_jar-from swing-layout-1
	java-pkg_ensure-no-bundled-jars
}

src_compile() {
	eant linux

	# Make sure that only mocha code goes into the .jar
	rm -Rf build/{com/sun,javax} || die

	# FIXME: Currently bundling system's dev-java/swing-layout
	# rm -Rf build/org || die

	eant jar
}

src_install() {
	java-pkg_dojar build/${PN}.jar
	java-pkg_dolauncher
}
