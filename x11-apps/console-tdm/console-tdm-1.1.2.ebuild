# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eutils

DESCRIPTION="Console display manager based on CDM"
HOMEPAGE="https://dopsi.github.io/console-tdm/"
SRC_URI="https://github.com/dopsi/console-tdm/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-util/dialog
	x11-apps/xinit
	"

DOCS=( ChangeLog.md NEWS.md README.md )

src_compile() { : ; }

src_install() {
	emake DESTDIR="${D}" PREFIX=/usr install
	einstalldocs
}
