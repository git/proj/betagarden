# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit distutils-r1

MY_PN="imagescaler"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Gamma-correct image scaler"
HOMEPAGE="http://git.goodpoint.de/?p=imagescaler.git;a=summary"
SRC_URI="http://www.hartwork.org/public/${MY_P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="media-gfx/gimp"

S="${WORKDIR}"/${MY_P}
