# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"

MY_PN="gluas"
MY_P="${MY_PN}-${PV}"

DESCRIPTION="Plug-in adding Lua scripting to the GIMP"
HOMEPAGE="http://pippin.gimp.org/plug-ins/gluas/"
SRC_URI="http://pippin.gimp.org/plug-ins/${MY_PN}/files/${MY_P}.tar.gz"

LICENSE="|| ( GPL-2 GPL-3 )"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-lang/lua
	media-gfx/gimp
	x11-libs/gtk+:2
	|| ( x11-libs/gtksourceview:2.0
		x11-libs/gtksourceview:1.0 )"
RDEPEND="${DEPEND}"

S="${WORKDIR}"/${MY_P}

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc BUGS NEWS HACKING TODO ChangeLog AUTHORS README || die
}
