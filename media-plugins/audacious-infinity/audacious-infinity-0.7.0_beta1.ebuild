# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

MY_PV="${PV/_/}"
MY_P="infinity-plugin-4-audacious-${MY_PV}"

DESCRIPTION="A psychedelic visualization plug-in for XMMS"
HOMEPAGE="http://infinity-plugin.sourceforge.net"
SRC_URI="mirror://sourceforge/infinity-plugin/${MY_P}.tar.gz"

SLOT="0"
LICENSE="GPL-2"
#-sparc: 0.2: always black - eradicator
KEYWORDS="~amd64 ~ppc ~x86"
IUSE=""

DEPEND="
	media-libs/libsdl
	media-sound/audacious"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${MY_P}"
