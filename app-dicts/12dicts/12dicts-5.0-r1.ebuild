# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

inherit eutils

DESCRIPTION="Collection of English word lists"
HOMEPAGE="http://wordlist.aspell.net/12dicts/"
SRC_URI="mirror://sourceforge/project/wordlist/12Dicts/${PV}/${P}.zip"

LICENSE="wordlist"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/unzip"

S="${WORKDIR}"

src_prepare() {
	edos2unix *
}

src_install() {
	insinto /usr/share/${PN}
	doins *.txt

	dodoc *.html
}
