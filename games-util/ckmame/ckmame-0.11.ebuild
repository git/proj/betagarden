# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="4"

DESCRIPTION="Program to check ROM sets for MAME"
HOMEPAGE="http://nih.at/ckmame/"
SRC_URI="http://nih.at/${PN}/${P}.tar.bz2"

LICENSE="GPL-2+ BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="sys-libs/zlib
	>=dev-libs/libzip-0.9
	dev-db/sqlite:3
	dev-libs/libxml2"
DEPEND="virtual/pkgconfig
	virtual/man
	${RDEPEND}"
