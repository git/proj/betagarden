# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Single-file RC4-based encrypted file system for FUSE"
HOMEPAGE="http://www.enderunix.org/metfs/"
SRC_URI="http://www.enderunix.org/${PN}/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

RDEPEND="
	sys-fs/fuse
	dev-libs/libgcrypt:0=
	dev-libs/libtar"
DEPEND="${RDEPEND}"

src_prepare() {
	default
	sed \
		-e 's|/usr/local/lib/libtar.a|-ltar|' \
		-i configure || die "sed failed"
}

src_install() {
	dodoc AUTHORS ChangeLog README TODO
	dobin metfs
}
