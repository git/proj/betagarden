# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=4

inherit toolchain-funcs

MY_P=${P/-/_}

DESCRIPTION="Tool to recover deleted files"
HOMEPAGE="http://www.giis.co.in/giis/"
SRC_URI="http://www.giis.co.in/giis/${MY_P}.tar"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="sys-fs/e2fsprogs"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

S="${WORKDIR}"/${MY_P}

src_prepare() {
	sed -i \
		-e '/^CFLAGS/d' \
		-e 's/LDFLAGS =/LDFLAGS +=/' \
		-e "s:gcc:$(tc-getCC):" \
		-e 's:$(LDFLAGS) src/extcarve.c:src/extcarve.c $(LDFLAGS):' \
		Makefile || die
}

src_install() {
	dobin extcarve
	dodoc README HOWTO_CUSTOMIZE
}
