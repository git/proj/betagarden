#! /usr/bin/env bash
# Copyright (C) 2010-2013 Sebastian Pipping <sebastian@pipping.org>
# Licensed under GPL v2 or later
#
# Purpose:
#   Makes a wrapfs snapshot tarball to allow distribution as a standlone kernel module
#
# Usage:
#   $ ./files/extract-module-snapshot-tarball.sh

die() {
	echo $@
	exit 1
}


# Fetch latest sources from Git
if [[ -d .wrapfs-latest ]]; then
	cd .wrapfs-latest || die
	git remote update || die
	git checkout master || die
	git reset --hard origin/master || die
	echo
else
	git clone --depth 1 git://git.fsl.cs.sunysb.edu/wrapfs-latest.git .wrapfs-latest || die
	echo
	cd .wrapfs-latest || die
fi


# Ensure usable kernel version string
if [[ ! -e include/config/kernel.release ]]; then
	if [[ ! -e .config ]]; then
		make allnoconfig || die
	fi
	make include/config/kernel.release || die
	echo
fi


VERSION=$(/bin/grep '^WRAPFS_VERSION=' fs/wrapfs/Makefile | sed 's/WRAPFS_VERSION="\(.\+\)"/\1/')
[ $(wc -l <<<"${VERSION}") == 1 ] || die 'grepping WRAPFS_VERSION failed'
VERSION="${VERSION}_p$(date +'%Y%m%d')"
DISTNAME="wrapfs-${VERSION}"
TARBALL_NAME="${DISTNAME}".tar


# Reset
if [[ -n "${DISTNAME}" && -d "${DISTNAME}" ]]; then
	echo 'Cleaning up...'
	rm -Rfv "${DISTNAME}"
	echo
fi
mkdir -p "${DISTNAME}" || die 'mkdir failed'


# Code
cp fs/wrapfs/*.{c,h} "${DISTNAME}"/ || die 'cp failed'

# Patch: Integrate WRAPFS_SUPER_MAGIC
wrapfs_super_magic_line=$(fgrep WRAPFS_SUPER_MAGIC include/uapi/linux/magic.h)
[ $(wc -l <<<"${wrapfs_super_magic_line}") == 1 ] || die 'grepping WRAPFS_SUPER_MAGIC failed'
sed 's|^#include "wrapfs.h"|&\n'"${wrapfs_super_magic_line}"'|' -i "${DISTNAME}"/super.c

# Patch: Turn into module, update version
sed \
	-e 's|^WRAPFS_VERSION=.*|WRAPFS_VERSION="'"${VERSION}"'"|' \
	-e 's|$(CONFIG_WRAP_FS)|m|' \
	fs/wrapfs/Makefile \
	> "${DISTNAME}"/Makefile

# Docs
cp Documentation/filesystems/wrapfs.txt "${DISTNAME}"/README || die 'cp failed'
cp include/config/kernel.release "${DISTNAME}"/LINUX_VERSION || die 'cp failed'

# Author and contact info
fgrep -A6 WRAPFS MAINTAINERS > "${DISTNAME}"/AUTHORS

# Install instructions
cat <<-EOF >"${DISTNAME}"/INSTALL
	tar -xvjf ${DISTNAME}.tar.xz
	cd ${DISTNAME}
	make -C /lib/modules/\`uname -r\`/build M=\`pwd\` modules
	sudo make -C /lib/modules/\`uname -r\`/build M=\`pwd\` modules_install
	sudo insmod /lib/modules/`uname -r`/extra/wrapfs.ko
EOF


# Archive
echo 'Archiving...'
rm -f ../"${TARBALL_NAME}"{,.xz}
tar -cvf ../"${TARBALL_NAME}" "${DISTNAME}"
xz ../"${TARBALL_NAME}"
echo

target_kernel=$(cat include/config/kernel.release)
echo "Done.  Target kernel is \"${target_kernel}\", tarball created at \"${TARBALL_NAME}.xz\"."
