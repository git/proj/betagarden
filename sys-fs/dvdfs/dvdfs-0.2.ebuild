# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="3"

inherit eutils

DESCRIPTION="Mounts a DVD using libdvdread"
HOMEPAGE="http://jspenguin.org:81/software/dvdfs/"
SRC_URI="http://jspenguin.org:81/software/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="media-libs/libdvdread
	sys-fs/fuse"
DEPEND="${DEPEND}
	virtual/pkgconfig"

src_prepare() {
	epatch "${FILESDIR}"/${P}-makefile.patch
}

src_install() {
	dobin dvdfs || die
	dobin README || die
}
