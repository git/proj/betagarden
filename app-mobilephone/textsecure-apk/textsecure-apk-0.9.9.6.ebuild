# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

inherit java-pkg-2 java-ant-2

ABS_PN=ActionBarSherlock
ABS_PV=4.2.0
ABS_P=${ABS_PN}-${ABS_PV}

MY_PN=TextSecure
MY_P=${MY_PN}-${PV}
DESCRIPTION="A secure text messaging application for Android"
HOMEPAGE="https://github.com/WhisperSystems/TextSecure"
SRC_URI="
	https://github.com/WhisperSystems/${MY_PN}/archive/v${PV}.tar.gz -> ${MY_P}.tar.gz
	https://github.com/JakeWharton/${ABS_PN}/archive/${ABS_PV}.tar.gz -> ${ABS_P}.tar.gz"

LICENSE="GPL-3 Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	>=virtual/jdk-1.7:*
	dev-util/android-sdk-update-manager"
RDEPEND=">=virtual/jre-1.7"

S=${WORKDIR}

PROPERTIES=interactive

ANDROID_TARGET=android-15

android() {
	/opt/android-sdk-update-manager/tools/android "$@"
}

pkg_setup() {
	# Super-dirty.  We could prepend "echo y | " but we would be accepting a version
	# for the user then (that might have changed upstream in the mean time).
	android update sdk -u --filter platform-tools,${ANDROID_TARGET},extra-android-support,build-tools-17.0.0
}

src_prepare() {
	rm -R ${ABS_P}/samples || die

	find -type f -name android-support-v4.jar | while read dest ; do
		rm "${dest}" || die
		ln -s /opt/android-sdk-update-manager/extras/android/support/v4/android-support-v4.jar "${dest}" || die
	done

	ewarn 'These .jar files might still need to be unbundled:'
	ewarn "$(find -type f -name '*.jar' | sort)"
}

src_configure() {
	# NOTE: --library needs a relative path
	android update project --path "${WORKDIR}"/${ABS_P}/library --target ${ANDROID_TARGET}
	android update project --path "${WORKDIR}"/${MY_P} --target ${ANDROID_TARGET} --library ../${ABS_P}/library
}

src_compile() {
	# Dirty sandbox workaround to make keytool work
	local actual_home="$(sh -c "echo ~$(id -n -u)")"
	addwrite "${actual_home}"/.android
	addread "${actual_home}"/.android

	cd "${MY_P}" || die
	eant debug
}

src_install() {
	insinto /usr/share/${MY_PN,,}-${PV}
	doins "${MY_P}"/bin/${MY_PN}-debug.apk
	doins "${MY_P}"/bin/${MY_PN}-debug-unaligned.apk
}
