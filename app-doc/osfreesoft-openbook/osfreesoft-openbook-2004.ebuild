# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="O'Reilly Open Book 'Understanding Open Source and Free Software Licensing'"
HOMEPAGE="http://www.oreilly.com/openbook/osfreesoft/book/"
SRC_URI="
	http://akamaicovers.oreilly.com/images/9780596005818/lrg.jpg
	http://www.oreilly.com/openbook/osfreesoft/book/appa.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/toc.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/ch00.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/ch01.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/ch02.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/ch03.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/ch04.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/ch05.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/ch06.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/ch07.pdf
	http://www.oreilly.com/openbook/osfreesoft/book/inx.pdf"

LICENSE="CC-BY-ND-2.5"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE="bindist"

RDEPEND=""
DEPEND="
	app-text/pdfjam
	media-gfx/imagemagick"

S="${WORKDIR}"

src_unpack() {
	true
}

src_compile() {
	convert "${DISTDIR}"/0596005814_lrg.jpg cover.pdf || die "convert failed"

	if ! use bindist ; then
		# Joining chapters documents together could be considered
		# a derivative work so we cannot distribute it
		pdfjoin --fitpaper false --paper letterpaper --outfile all.pdf \
			cover.pdf "${DISTDIR}"/{toc,ch0[0-7],appa,inx}.pdf || die
	fi
}

src_install() {
	insinto /usr/share/doc/${PF}/pdf/
	use bindist || doins all.pdf
	doins cover.pdf "${DISTDIR}"/{toc,ch0[0-7],appa,inx}.pdf
}
