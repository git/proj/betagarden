# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="2"

DESCRIPTION="O'Reilly Open Book 'Managing Projects with GNU Make Managing Projects with GNU Make, Third Edition'"
HOMEPAGE="http://oreilly.com/catalog/make3/book/index.csp"

SRC_URI="http://covers.oreilly.com/images/9780596006105/lrg.jpg -> ${PN}-cover.jpg"
FRAGMENTS=( cpyrt toc author_colo part1 ch0{0..5} part2 ch{0{6..9},1{1..2}} part3 app{a,b,c} inx)
for i in ${FRAGMENTS[@]} ; do
	SRC_URI+=" http://oreilly.com/catalog/make3/book/${i}.pdf -> ${PN}-${i}.pdf"
done
PDF_BASENAMES=( ${FRAGMENTS[@]/%/.pdf} )
PDF_FILES=( "${PDF_BASENAMES[@]/#/${DISTDIR}/${PN}-}" )

LICENSE="FDL-1.1"
SLOT="0"
KEYWORDS="amd64 mips ppc s390 sh x86"
IUSE="bindist"

RDEPEND=""

DEPEND="app-text/pdfjam"

src_unpack() {
	true
}

S="${WORKDIR}"

src_compile() {
	# Workaround: Rotate to make pdfjoin work right after
	for i in "${PDF_FILES[@]}"; do
		pdf270 "${i}" || die
	done

	local INPUT_FILES=( ${FRAGMENTS[@]/%/-rotated270.pdf} )
	INPUT_FILES=( "${DISTDIR}"/${PN}-cover.jpg ${INPUT_FILES[@]/#/${PN}-} )

	pdfjoin --fitpaper 'true' --outfile ${PN}.pdf "${INPUT_FILES[@]}" || die
}

src_install() {
	insinto /usr/share/doc/${PF}/pdf/
	doins ${PN}.pdf || die
}
