# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

PYTHON_COMPAT=( python2_7 )

inherit python-r1

DESCRIPTION="An Encoder/Decoder Using The 'One-Time Pad' Method"
HOMEPAGE="http://www.red-bean.com/onetime/"
SRC_URI="http://www.red-bean.com/${PN}/${P}.tar.gz"

LICENSE="CC-PD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${PYTHON_DEPS}"
DEPEND="${DEPEND}"

src_compile() {
	:
}

src_install() {
	python_foreach_impl python_doscript ${PN}
	dodoc README
}
