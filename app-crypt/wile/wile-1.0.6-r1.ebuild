# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6,7} )
DISTUTILS_USE_SETUPTOOLS=rdepend

inherit distutils-r1

DESCRIPTION="Stripped down letsencrypt (ACME) client"
HOMEPAGE="https://github.com/costela/wile"
SRC_URI="https://github.com/costela/wile/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	dev-python/pytest-runner[${PYTHON_USEDEP}]
	dev-python/setuptools_scm[${PYTHON_USEDEP}]
	test? ( dev-python/testfixtures[${PYTHON_USEDEP}] )
	"
RDEPEND="
	>app-crypt/acme-0.22[${PYTHON_USEDEP}]
	dev-python/josepy[${PYTHON_USEDEP}]
	dev-python/six[${PYTHON_USEDEP}]
	<dev-python/click-7[${PYTHON_USEDEP}]
	dev-python/pyopenssl[${PYTHON_USEDEP}]
	dev-python/cryptography[${PYTHON_USEDEP}]
	dev-python/paramiko[${PYTHON_USEDEP}]
	"

DOCS=( README.md )

RESTRICT="test"

distutils_enable_tests pytest
