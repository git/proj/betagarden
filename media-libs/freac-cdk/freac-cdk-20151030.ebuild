# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="fre:ac Component Development Kit"
HOMEPAGE="https://www.freac.org/"
SRC_URI="https://freac.org/preview/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-libs/libcdio
	dev-libs/libcdio-paranoia
	media-libs/alsa-lib
	media-libs/flac
	media-libs/libvorbis
	media-libs/opus
	media-libs/speex
	sys-devel/gcc:*[cxx]
	x11-libs/smooth
	"
DEPEND="${RDEPEND}"

src_compile() {
	emake prefix=/usr
}

src_install() {
	addpredict /etc/ld.so.cache:/etc/ld.so.cache~

	emake DESTDIR="${D}" prefix=/usr install
}
