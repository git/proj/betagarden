# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

DESCRIPTION="fre:ac Component Development Kit"
HOMEPAGE="https://www.freac.org/"
SRC_URI="https://freac.org/preview/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	dev-libs/expat
	dev-libs/libcdio
	dev-libs/libcdio-paranoia
	dev-libs/uriparser
	media-libs/alsa-lib
	media-libs/flac
	media-libs/libvorbis
	media-libs/libxspf
	media-libs/opus
	media-libs/speex
	sys-devel/gcc:*[cxx]
	sys-libs/zlib
	x11-libs/smooth
	"
DEPEND="${RDEPEND}"

src_compile() {
	local config=(
		systemlibexpat
		systemliburiparser
		systemlibxspf
		systemzlib
	)

	emake prefix=/usr config="${config[*]}"
}

src_install() {
	emake DESTDIR="${D}" prefix=/usr install
}
